<?php

namespace Drupal\ptools\Pager;

use Drupal\ptools\Repository\Range;

/**
 * Common interface for pager handlers.
 */
interface PagerHandlerInterface {

  /**
   * Creates a pager based on the values in the global state.
   *
   * @param int|null $limit
   *   (optional) A fallback value to use for limit, if none is found in the
   *   global state. Defaults to the configured default value.
   * @param int $id
   *   (optional) The pager identifier. Defaults to 0.
   *
   * @return \Drupal\ptools\Repository\Range
   *   A range object.
   */
  public function getRangeFromParameters(int $limit = NULL, int $id = 0): Range;

  /**
   * Creates a pager based on the specified page.
   *
   * @param int $page
   *   The page number.
   * @param int|null $limit
   *   (optional) The slice size. Defaults to the configured default value.
   * @param int $id
   *   (optional) The pager identifier. Defaults to 0.
   *
   * @return \Drupal\ptools\Repository\Range
   *   A range object.
   */
  public function getRangeFromPage(int $page, int $limit = NULL, int $id = 0): Range;

  /**
   * Generates a pager element corresponding to the specified range.
   *
   * @param \Drupal\ptools\Repository\Range
   *   A range object.
   *
   * @return array
   *   A pager render array.
   */
  public function buildPager(Range $range): array;

}
