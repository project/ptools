<?php

namespace Drupal\ptools\Pager;

use Drupal\Core\Pager\PagerManagerInterface;
use Drupal\Core\Pager\PagerParametersInterface;
use Drupal\Core\Site\Settings;
use Drupal\ptools\Repository\Range;

/**
 * Default pager handler implementation.
 */
class PagerHandler implements PagerHandlerInterface {

  /**
   * The pager manager.
   *
   * @var \Drupal\Core\Pager\PagerManagerInterface
   */
  protected $pagerManager;

  /**
   * The pager parameters.
   *
   * @var \Drupal\Core\Pager\PagerParametersInterface
   */
  protected $pagerParameters;

  /**
   * Constructs a pager.
   *
   * @param \Drupal\Core\Pager\PagerManagerInterface $pager_manager
   *   The pager manager.
   * @param \Drupal\Core\Pager\PagerParametersInterface $pager_parameters
   *   The pager parameters.
   */
  public function __construct(PagerManagerInterface $pager_manager, PagerParametersInterface $pager_parameters) {
    $this->pagerManager = $pager_manager;
    $this->pagerParameters = $pager_parameters;
  }

  /**
   * {@inheritdoc}
   */
  public function getRangeFromParameters(int $limit = NULL, int $id = 0): Range {
    $page = $this->pagerParameters->findPage($id);
    return $this->getRangeFromPage($page, $limit, $id);
  }

  /**
   *{@inheritdoc}
   */
  public function getRangeFromPage(int $page, int $limit = NULL, int $id = 0): Range {
    if (!isset($limit)) {
      $limit = Settings::get('ptools_pager_limit', 25);
    }
    return new Range($page * $limit, $limit, $id);
  }

  /**
   *{@inheritdoc}
   */
  public function buildPager(Range $range): array {
    $this->pagerManager->createPager($range->getTotal(), $range->getLimit(), $range->getId());
    return [
      '#type' => 'pager',
      '#element' => $range->getId(),
    ];
  }

}
