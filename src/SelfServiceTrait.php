<?php

namespace Drupal\ptools;

/**
 * Trait allowing a service named after its class to self-instantiate.
 */
trait SelfServiceTrait {

  /**
   * Returns the singleton instance.
   *
   * @return static
   *   The singleton instance.
   */
  public static function get(): object {
    $instance = \Drupal::service(static::class);
    assert(is_a($instance, static::class));
    return $instance;
  }

}
