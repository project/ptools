<?php

namespace Drupal\ptools\EventSubscriber;

use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\PageCache\ResponsePolicy\KillSwitch;
use Drupal\Core\Routing\LocalRedirectResponse;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\ptools\Handler\SubrequestHandler;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * Base class for request event subscribers performing sub-requests.
 */
abstract class SubrequestEventSubscriberBase implements EventSubscriberInterface {

  protected const CACHE_KILLSWITCH_ATTRIBUTE = 'ptools.disable_all_caching';

  /**
   * The current route match.
   *
   * @var \Drupal\Core\Routing\RouteMatchInterface
   */
  protected RouteMatchInterface $routeMatch;

  /**
   * The subrequest handler.
   *
   * @var \Drupal\ptools\Handler\SubrequestHandler
   */
  protected SubrequestHandler $subrequestHandler;

  /**
   * Page cache kill switch
   *
   * @var \Drupal\Core\PageCache\ResponsePolicy\KillSwitch
   */
  protected KillSwitch $killSwitch;

  /**
   * SubrequestEventSubscriberBase constructor.
   */
  public function __construct(RouteMatchInterface $route_match, SubrequestHandler $subrequest_handler, KillSwitch $kill_switch) {
    $this->routeMatch = $route_match;
    $this->subrequestHandler = $subrequest_handler;
    $this->killSwitch = $kill_switch;
  }

  /**
   * Acts on HTTP request reception.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The request event.
   */
  public function onRequest(RequestEvent $event): void {
    // We currently support only GET sub-requests.
    $request = $event->getRequest();
    $is_get_request = strtoupper($request->getMethod()) === 'GET';
    $response = $is_get_request ? $this->getResponse($event) : NULL;
    if ($response) {
      $event->setResponse($response);
    }
    if (!$is_get_request) {
      // Prevent other request types from poisoning page cache.
      $this->killSwitch->trigger();
      // This is used in ::onResponse.
      $request->attributes->set(static::CACHE_KILLSWITCH_ATTRIBUTE, TRUE);
    }
  }

  /**
   * Reacts to a response.
   *
   * @param ResponseEvent $event
   *   The response event.
   */
  public function onResponse(ResponseEvent $event): void {
    if (!$event->getRequest()->attributes->get(static::CACHE_KILLSWITCH_ATTRIBUTE)) {
      return;
    }

    // Prevent the response itself being cached on reverse proxies.
    $response = $event->getResponse();
    $response->setPrivate();
    $response->headers->set('Cache-Control', 'no-cache, must-revalidate');

    if ($response instanceof CacheableResponseInterface) {
      $response->getCacheableMetadata()->setCacheMaxAge(0);
    }
    $event->setResponse($response);
  }

  /**
   * Retrieves a response overriding the current controller.
   *
   * @param \Symfony\Component\HttpKernel\Event\RequestEvent $event
   *   The request event.
   *
   * @return \Symfony\Component\HttpFoundation\Response|null
   *   An HTTP response or NULL if the current controller should not be
   *   overridden.
   */
  abstract protected function getResponse(RequestEvent $event): ?Response;

  /**
   * Returns an uncacheable redirect response.
   *
   * @param string $url
   *   The destination URL.
   * @param int $status
   *   (optional) The redirect HTTP status. Defaults to permanently moved.
   *
   * @return \Drupal\Core\Routing\LocalRedirectResponse
   *   A redirect response.
   */
  protected function getUncacheableRedirectResponse(string $url, $status = Response::HTTP_MOVED_PERMANENTLY): LocalRedirectResponse {
    $response = new LocalRedirectResponse($url, $status);
    // Make the response uncacheable to avoid redirect loops.
    $response->addCacheableDependency(new \stdClass());
    $response->setPrivate();
    $response->headers->addCacheControlDirective('no-store');
    return $response;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      KernelEvents::REQUEST => 'onRequest',
      KernelEvents::RESPONSE => 'onResponse',
    ];
  }

}
