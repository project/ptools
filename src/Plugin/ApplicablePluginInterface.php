<?php

namespace Drupal\ptools\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Common interface for applicable plugins.
 */
interface ApplicablePluginInterface extends PluginInspectionInterface {

  /**
   * Checks whether this plugin is applicable to the specified context.
   *
   * @param \Drupal\ptools\Plugin\ApplicablePluginContextInterface $context
   *   Data useful to determine applicability.
   *
   * @return bool
   *   TRUE if the plugin applies to the specified context, FALSE otherwise.
   */
  public function appliesTo(ApplicablePluginContextInterface $context): bool;

}
