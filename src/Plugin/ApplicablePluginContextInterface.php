<?php

namespace Drupal\ptools\Plugin;

/**
 * Interface for applicable plugin context data.
 */
interface ApplicablePluginContextInterface {}
