<?php

declare(strict_types=1);

namespace Drupal\ptools\Plugin;

/**
 * Base class for plugins.
 */
abstract class WeightedPluginBase extends PluginBase implements WeightedPluginInterface {

  /**
   * {@inheritdoc}
   */
  public function getWeight(): int {
    return (int) ($this->pluginDefinition['weight'] ?? 0);
  }

}
