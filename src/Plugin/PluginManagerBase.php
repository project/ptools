<?php

declare(strict_types=1);

namespace Drupal\ptools\Plugin;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Generic plugin factory.
 */
abstract class PluginManagerBase extends DefaultPluginManager implements PluginManagerInterface {

  /**
   * The available plugin instances.
   *
   * @var \Drupal\Component\Plugin\PluginInspectionInterface[]
   */
  protected array $instances;

  /**
   * {@inheritdoc}
   */
  public function getInstances(): array {
    if (!isset($this->instances)) {
      $this->instances = [];

      $weighted = TRUE;
      foreach ($this->getDefinitions() as $plugin_id => $definition) {
        try {
          $instance = $this->createInstance($plugin_id);
          $this->instances[$plugin_id] = $instance;
          if (!$instance instanceof WeightedPluginInterface) {
            $weighted = FALSE;
          }
        }
        catch (PluginException $e) {
          assert(FALSE);
        }
      }

      if ($weighted) {
        uksort($this->instances, function ($a_key, $b_key) {
          assert($this->instances[$a_key] instanceof WeightedPluginInterface);
          assert($this->instances[$b_key] instanceof WeightedPluginInterface);
          return $this->instances[$a_key]->getWeight() - $this->instances[$b_key]->getWeight();
        });
      }
    }

    return $this->instances;
  }

}
