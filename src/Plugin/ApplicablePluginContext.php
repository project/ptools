<?php

declare(strict_types=1);

namespace Drupal\ptools\Plugin;

use Drupal\ptools\Context\Context;

/**
 * Basic applicable plugin context implementation.
 */
class ApplicablePluginContext extends Context implements ApplicablePluginContextInterface {
}
