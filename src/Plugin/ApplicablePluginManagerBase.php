<?php

declare(strict_types=1);

namespace Drupal\ptools\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;
use Drupal\ptools\Exception\InvalidArgumentException;

/**
 * Applicable plugin manager.
 */
abstract class ApplicablePluginManagerBase extends PluginManagerBase implements ApplicablePluginManagerInterface {

  /**
   * {@inheritdoc}
   */
  public function getApplicablePlugins(ApplicablePluginContextInterface $context): array {
    $plugins = [];
    foreach ($this->getInstances() as $plugin) {
      if ($this->isPluginApplicable($plugin, $context)) {
        $plugins[] = $plugin;
      }
    }
    if ($plugins) {
      return $plugins;
    }
    throw new InvalidArgumentException($context);
  }

  /**
   * {@inheritdoc}
   */
  public function getFirstApplicablePlugin(ApplicablePluginContextInterface $context): ApplicablePluginInterface {
    foreach ($this->getInstances() as $plugin) {
      if ($this->isPluginApplicable($plugin, $context)) {
        return $plugin;
      }
    }
    throw new InvalidArgumentException($context);
  }

  /**
   * Check whether this plugin is applicable to the specified context.
   *
   * @param \Drupal\Component\Plugin\PluginInspectionInterface $plugin
   *   A plugin instance.
   * @param \Drupal\ptools\Plugin\ApplicablePluginContextInterface $context
   *   Data useful to determine applicability.
   *
   * @return bool
   *   TRUE if the plugin applies to the specified context, FALSE otherwise.
   */
  protected function isPluginApplicable(PluginInspectionInterface $plugin, ApplicablePluginContextInterface $context): bool {
    return $plugin instanceof ApplicablePluginInterface && $plugin->appliesTo($context);
  }

}
