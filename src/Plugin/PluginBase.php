<?php

declare(strict_types=1);

namespace Drupal\ptools\Plugin;

use Drupal\Component\Plugin\PluginBase as ComponentPluginBase;

/**
 * Base class for plugins.
 */
abstract class PluginBase extends ComponentPluginBase {

  /**
   * PluginBase constructor.
   *
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param array $plugin_definition
   *   The plugin implementation definition.
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   */
  public function __construct(string $plugin_id, array $plugin_definition, array $configuration = []) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

}
