<?php

namespace Drupal\ptools\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Common interface for plugins.
 */
interface WeightedPluginInterface extends PluginInspectionInterface {

  /**
   * Returns the plugin weight.
   *
   * @return int
   *   The plugin weight.
   */
  public function getWeight(): int;

}
