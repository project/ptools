<?php

namespace Drupal\ptools\Plugin;

use Drupal\Component\Plugin\PluginManagerInterface as ComponentPluginManagerInterface;

/**
 * Common interface for generic plugin factories.
 */
interface PluginManagerInterface extends ComponentPluginManagerInterface {

  /**
   * Returns all available plugin instances.
   *
   * @return \Drupal\Component\Plugin\PluginInspectionInterface[]
   *   An array of plugin instances.
   */
  public function getInstances(): array;

}
