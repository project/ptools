<?php

namespace Drupal\ptools\Plugin;

use Drupal\Component\Plugin\PluginManagerInterface as ComponentPluginManagerInterface;

/**
 * Applicable plugin manager interface.
 */
interface ApplicablePluginManagerInterface extends ComponentPluginManagerInterface {

  /**
   * Returns a set of plugin instances suitable for the specified context.
   *
   * @param \Drupal\ptools\Plugin\ApplicablePluginContextInterface $context
   *   Data useful to determine applicability.
   *
   * @return \Drupal\ptools\Plugin\ApplicablePluginInterface[]
   *   An array of plugin instances matching the specified context.
   *
   * @throws \Drupal\ptools\Exception\InvalidArgumentException
   *   If no matching plugin instance was found.
   */
  public function getApplicablePlugins(ApplicablePluginContextInterface $context): array;

  /**
   * Returns the first plugin instance suitable for the specified context.
   *
   * @param \Drupal\ptools\Plugin\ApplicablePluginContextInterface $context
   *   Data useful to determine applicability.
   *
   * @return \Drupal\ptools\Plugin\ApplicablePluginInterface
   *   A plugin instance matching the specified context.
   *
   * @throws \Drupal\ptools\Exception\InvalidArgumentException
   *   If no matching plugin instance was found.
   */
  public function getFirstApplicablePlugin(ApplicablePluginContextInterface $context): ApplicablePluginInterface;

}
