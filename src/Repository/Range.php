<?php

namespace Drupal\ptools\Repository;

/**
 * Represents a result set range.
 */
class Range {

  /**
   * An arbitrary identifier for this range.
   *
   * @var string
   */
  protected $id = 0;

  /**
   * The desired offset.
   *
   * @var int
   */
  protected $offset = 0;

  /**
   * The slice size.
   *
   * @var int
   */
  protected $limit = 0;

  /**
   * The result set size.
   *
   * @var int
   */
  protected $total = 0;

  /**
   * Constructs a range.
   *
   * @param int $offset
   *   The desired offset.
   * @param int $limit
   *   The slice size.
   */
  public function __construct(int $offset, int $limit, string $id = NULL) {
    $this->id = $id ?: 0;
    $this->offset = $offset;
    $this->limit = $limit;
  }

  /**
   * Returns the range ID.
   *
   * @return string
   *   An arbitrary identifier string.
   */
  public function getId(): string {
    return $this->id;
  }

  /**
   * Returns the total number of items in the result set.
   *
   * @return int
   *   The offset.
   */
  public function getOffset(): int {
    return $this->offset;
  }

  /**
   * Returns the total number of items in the result set.
   *
   * @return int
   *   The limit.
   */
  public function getLimit(): int {
    return $this->limit;
  }

  /**
   * Returns the total number of items in the result set.
   *
   * @return int
   *   The total.
   */
  public function getTotal(): int {
    return $this->total;
  }

  /**
   * Sets the total number of items in the result set.
   *
   * @param int $total
   *   The total.
   */
  public function setTotal(int $total): void {
    $this->total = $total;
  }

  /**
   * Returns whether the result set has more items.
   *
   * @return bool
   *   TRUE if more items can be returned, FALSE otherwise.
   */
  public function hasMore(): bool {
    return $this->offset + $this->limit < $this->total;
  }

}
