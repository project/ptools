<?php

namespace Drupal\ptools;

/**
 * Trait useful to reuse Drupal's static cache in classes.
 *
 * @see \drupal_static()
 * @see \drupal_static_reset()
 */
trait StaticCacheTrait {

  /**
   * A collection of fetched cache items.
   *
   * @var bool[]
   */
  protected $fetchedCacheItems = [];

  /**
   * Stores a unique ID for the current object.
   *
   * @var string
   */
  protected $objectId;

  /**
   * Returns a statically cached item.
   *
   * @param string $name
   *   A locally unique cache item name.
   * @param mixed $default
   *   (optional) Default value.
   *
   * @return mixed
   *   A reference to the cache item.
   */
  protected function &getStaticCache($name, $default = NULL) {
    $this->fetchedCacheItems[$name] = TRUE;
    $key = $this->getStaticCacheKey($name);
    return drupal_static($key, $default);
  }

  /**
   * Resets the specified static cache item.
   *
   * @param string $name
   *   (optional) A locally unique cache item name. If none is provided all
   *   cached items are cleared.
   */
  public function resetStaticCache($name = NULL) {
    if (isset($name)) {
      $key = $this->getStaticCacheKey($name);
      drupal_static_reset($key);
    }
    else {
      foreach ($this->fetchedCacheItems as $name => $fetched) {
        $this->resetStaticCache($name);
      }
    }
  }

  /**
   * Returns the static cache key for the specified name.
   *
   * @param string $name
   *   A cache item name.
   *
   * @return string
   *   A fully-qualified cache key.
   */
  protected function getStaticCacheKey($name) {
    if (!isset($this->objectId)) {
      $this->objectId = uniqid('ptools-');
    }
    return $this->objectId . ':' . static::class . '::' . $name;
  }

  /**
   * Performs a statically-cached operation.
   *
   * @param string|array $cid
   *   The cache ID.
   * @param callable $callback
   *   A callable executing the operation and fetching the result.
   *
   * @return mixed
   *   The callback result.
   */
  protected function doCachedCall($cid, callable $callback) {
    if (is_array($cid)) {
      $cid = implode(':', $cid);
    }
    $cache = &$this->getStaticCache(__FUNCTION__, []);
    if (!array_key_exists($cid, $cache)) {
      $cache[$cid] = $callback();
    }
    return $cache[$cid];
  }

}
