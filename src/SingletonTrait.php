<?php

namespace Drupal\ptools;

/**
 * Trait useful to implement the singleton pattern.
 */
trait SingletonTrait {

  /**
   * The singleton instance.
   *
   * @var static
   */
  protected static $instance;

  /**
   * Returns the singleton instance.
   *
   * @return static
   *   The singleton instance.
   */
  public static function get() {
    if (!isset(static::$instance)) {
      $definition = static::getDefinition();
      $instance = \Drupal::classResolver()->getInstanceFromDefinition($definition);
      if (!\Drupal::getContainer()->has($definition)) {
        assert(!class_exists($definition) || is_a($instance, $definition));
        static::$instance = $instance;
      }
    }
    else {
      $instance = static::$instance;
    }
    return $instance;
  }

  /**
   * Returns the class resolver definition for the current class.
   *
   * @return string
   *   A service name or the current class name.
   */
  protected static function getDefinition(): string {
    return static::class;
  }

  /**
   * Ensures that a new object will be instantiated the next time.
   */
  public static function reset() {
    static::$instance = NULL;
  }

}
