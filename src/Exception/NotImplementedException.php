<?php

namespace Drupal\ptools\Exception;

/**
 * Exception signaling that a method was not implemented.
 */
class NotImplementedException extends \LogicException {
}
