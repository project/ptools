<?php

namespace Drupal\ptools\Exception;

/**
 * More explicit invalid argument exception.
 */
class InvalidArgumentException extends \InvalidArgumentException {

  /**
   * InvalidArgumentException constructor.
   *
   * @param mixed $argument
   *   The invalid argument.
   */
  public function __construct($argument) {
    if (is_scalar($argument)) {
      $to_string = $argument;
    }
    elseif (is_array($argument)) {
      $to_string = 'array';
    }
    elseif (is_object($argument)) {
      $to_string = get_class($argument);
    }
    else {
      $to_string = print_r($argument, TRUE);
    }
    parent::__construct(sprintf('Invalid argument specified: %s', $to_string));
  }

}
