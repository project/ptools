<?php

namespace Drupal\ptools;

use Drupal\Core\Config\ConfigFactoryInterface;

/**
 * Trait useful to retrieve the configuration for the current class.
 */
trait ConfigFactoryTrait {

  /**
   * The configuration object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The configuration object name.
   *
   * @var string
   */
  protected $configName;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * Initializes the current configuration.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param string $config_name
   *   The configuration object name.
   */
  protected function initConfig(ConfigFactoryInterface $config_factory, string $config_name) {
    $this->configFactory = $config_factory;
    $this->configName = $config_name;
  }

  /**
   * Returns the current configuration.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   A configuration object.
   */
  protected function getConfig() {
    if (!isset($this->config)) {
      $this->config = $this->configFactory->get($this->configName);
    }
    return $this->config;
  }

}
