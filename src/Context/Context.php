<?php

namespace Drupal\ptools\Context;

/**
 * Provides a context.
 */
class Context implements \ArrayAccess {

  /**
   * The context data.
   *
   * @var array
   */
  protected array $state;

  /**
   * Context constructor.
   */
  public function __construct(array $state = []) {
    $this->state = $state;
  }

  /**
   * {@inheritdoc}
   */
  public function offsetExists($key): bool {
    assert(is_scalar($key));
    return isset($this->state[$key]);
  }

  /**
   * {@inheritdoc}
   */
  public function offsetGet($key): mixed {
    assert(is_scalar($key));
    return $this->state[$key] ?? NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function offsetSet($key, $value): void {
    assert(is_scalar($key));
    $this->state[$key] = $value;
  }

  /**
   * {@inheritdoc}
   */
  public function offsetUnset($key): void {
    assert(is_scalar($key));
    unset($this->state[$key]);
  }

  /**
   * Returns an array reference matching the specified key.
   *
   * @param string $key
   *   The context key.
   *
   * @return array
   *   An array reference.
   */
  public function &getValues(string $key): array {
    $values = &$this->state[$key];
    if (!isset($this->state[$key])) {
      $values = [];
    }
    assert(is_array($values));
    return $values;
  }

}
