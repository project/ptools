<?php

namespace Drupal\ptools\Context;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Provides a context stack.
 */
class ContextFactory implements ContextFactoryInterface {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * The context storage.
   *
   * @var \SplObjectStorage
   */
  protected \SplObjectStorage $data;

  /**
   * ContextFactory constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(RequestStack $request_stack) {
    $this->requestStack = $request_stack;
    $this->data = new \SplObjectStorage();
  }

  /**
   * {@inheritdoc}
   */
  public function getCurrentContext(): Context {
    $request = $this->requestStack->getCurrentRequest();
    assert($request instanceof Request);
    return $this->getContextFromRequest($request);
  }

  /**
   * {@inheritdoc}
   */
  public function getContextFromRequest(Request $request): Context {
    if (!isset($this->data[$request])) {
      // @todo Consider exposing the parent request context somehow.
      $context = new Context();
      $this->data[$request] = $context;
    }
    $context = $this->data[$request];
    return $context;
  }

}
