<?php

namespace Drupal\ptools\Context;

use Symfony\Component\HttpFoundation\Request;

/**
 * Common interface for context factories.
 */
interface ContextFactoryInterface {

  /**
   * Returns the current context.
   *
   * @return \Drupal\ptools\Context\Context
   *   The current context object.
   */
  public function getCurrentContext(): Context;

  /**
   * Returns the current context.
   *
   * @param \Symfony\Component\HttpFoundation\Request $request
   *   The request to get the context for.
   *
   * @return \Drupal\ptools\Context\Context
   *   The current context object.
   */
  public function getContextFromRequest(Request $request): Context;

}
