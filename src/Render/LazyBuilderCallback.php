<?php

namespace Drupal\ptools\Render;

use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\Core\Site\Settings;
use Drupal\ptools\Context\Context;

/**
 * Base class for lazy builders.
 */
class LazyBuilderCallback extends CallbackBase implements TrustedCallbackInterface {

  /**
   * LazyBuilderCallback constructor.
   *
   * @param string $class
   *   The view builder class.
   */
  public function __construct(string $class) {
    parent::__construct($class, 'build');
    assert(is_a($class, ViewBuilderInterface::class, TRUE));
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['lazyBuild'];
  }

  /**
   * Builds the lazy builder placeholder.
   *
   * @param \Drupal\ptools\Context\Context|null $context
   *   (optional) The lazy builder context. Defaults to none.
   *
   * @return array
   *   A render array.
   */
  public function build(Context $context = NULL): array {
    $serialized_context = $context ? serialize($context) : '';
    $hash = $context ? static::getContextHash($this->class, $serialized_context) : '';
    return [
      '#lazy_builder' => [
        static::class . '::lazyBuild',
        [$this->class, $serialized_context, $hash],
      ],
      '#create_placeholder' => TRUE,
    ];
  }

  /**
   * Computes the serialized context hash.
   *
   * @param string $class
   *   The view builder class name.
   * @param string $serialized_context
   *   The serialized context.
   *
   * @return string
   *   The computed hash.
   */
  protected static function getContextHash(string $class, string $serialized_context): string {
    $private_key = \Drupal::service('private_key')->get();
    $hash_salt = Settings::getHashSalt();
    return hash_hmac('sha256', $class . ':' . $serialized_context, $private_key . $hash_salt);
  }

  /**
   * Lazy-builder callback.
   */
  public static function lazyBuild(string $class, string $serialized_context, string $stored_hash): array {
    assert(!($serialized_context xor $stored_hash));
    $hash = $serialized_context ? static::getContextHash($class, $serialized_context) : '';
    if ($hash === $stored_hash) {
      $context = $serialized_context ? unserialize($serialized_context) : NULL;
      if (!isset($context) || $context instanceof Context) {
        $instance = static::doGetInstance($class);
        if ($instance instanceof ViewBuilderInterface) {
          return $instance->build($context);
        }
      }
    }
    throw new \RuntimeException('Invalid lazy builder context');
  }

}
