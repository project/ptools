<?php

namespace Drupal\ptools\Render;

use Drupal\Core\Security\DoTrustedCallbackTrait;
use Drupal\Core\Security\TrustedCallbackInterface;

/**
 * Class representing a serializable form callback.
 */
class PreRenderCallback extends CallbackBase implements TrustedCallbackInterface {

  use DoTrustedCallbackTrait;

  /**
   * Invokes the element pre-render method.
   *
   * @param array $element
   *   The element array.
   *
   * @return array|null
   *   The processed element.
   */
  public function invoke(array $element): array {
    return $this->doTrustedCallback([$this->getInstance(), $this->method], [$element], 'Invalid callback');
  }

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['invoke'];
  }

}
