<?php

namespace Drupal\ptools\Render;

use Drupal\ptools\Context\Context;

/**
 * Common interface for context-aware view builders.
 */
interface ViewBuilderInterface {

  /**
   * Builds a renderable element.
   *
   * @param \Drupal\ptools\Context\Context|null $context
   *   (optional) The renderable element context. Defaults to none.
   *
   * @return array
   *   The render array.
   */
  public function build(Context $context = NULL): array;

}
