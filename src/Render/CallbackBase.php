<?php

namespace Drupal\ptools\Render;

use Drupal\ptools\SingletonTrait;

/**
 * Base class for serializable render callbacks.
 */
class CallbackBase {

  /**
   * The processor class.
   *
   * @var string
   */
  protected $class;

  /**
   * The form processor method to be invoked.
   *
   * @var string
   */
  protected $method;

  /**
   * CallbackBase constructor.
   *
   * @param string $class
   *   The form processor class.
   * @param string $method
   *   The form processor method to be invoked.
   */
  public function __construct(string $class, string $method) {
    $this->class = $class;
    $this->method = $method;
  }

  /**
   * Returns a singleton class instance.
   *
   * @return object
   *   The singleton class instance.
   */
  protected function getInstance(): object {
    return static::doGetInstance($this->class);
  }

  /**
   * Actually returns a singleton class instance.
   *
   * @return object
   *   The singleton class instance.
   */
  protected static function doGetInstance(string $class): object {
    assert(method_exists($class, 'get') && in_array(SingletonTrait::class, class_uses($class)));
    $instance = $class::get();
    assert(get_class($instance) === $class);
    return $instance;
  }

}
