<?php

namespace Drupal\ptools\Render;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Block\BlockManagerInterface;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Cache\CacheableMetadata;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\ptools\CurrentUserTrait;
use Drupal\ptools\SingletonTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Block view builder.
 */
class BlockViewBuilder implements ContainerInjectionInterface {

  use CurrentUserTrait;
  use SingletonTrait;

  /**
   * @var \Drupal\Core\Block\BlockManagerInterface
   */
  protected $blockManager;

  /**
   * BlockViewBuilder constructor.
   */
  public function __construct(
    BlockManagerInterface $block_manager,
    AccountInterface $current_user
  ) {
    $this->blockManager = $block_manager;
    $this->currentUser = $current_user;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.block'),
      $container->get('current_user')
    );
  }

  /**
   * Renders a block.
   *
   * @param string $id
   *   The block ID.
   * @param array $config
   *   (optional) The block configuration.
   *
   * @return array
   *   A render array.
   */
  public function build(string $id, array $config = []): array {
    try {
      $block = $this->blockManager->createInstance($id, $config);
      assert($block instanceof BlockPluginInterface);
    }
    catch (PluginException $e) {
      assert(FALSE);
    }

    $access_result = $block->access($this->currentUser, TRUE);
    $build = $access_result->isAllowed() ? $block->build() : [];

    CacheableMetadata::createFromRenderArray($build)
      ->addCacheableDependency($access_result)
      ->applyTo($build);

    return $build;
  }

}
