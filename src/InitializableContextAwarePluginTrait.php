<?php

namespace Drupal\ptools;

use Drupal\Core\Plugin\ContextAwarePluginTrait;

/**
 * Trait useful to lazily initialize context-aware plugins.
 *
 * @see https://www.drupal.org/docs/drupal-apis/plugin-api/plugin-contexts
 */
trait InitializableContextAwarePluginTrait {

  use ContextAwarePluginTrait {
    getContextValue as parentGetContextValue;
  }

  /**
   * The context repository.
   *
   * @var \Drupal\Core\Plugin\Context\ContextRepositoryInterface
   */
  protected $contextRepository;

  /**
   * The context handler.
   *
   * @var \Drupal\Core\Plugin\Context\ContextHandlerInterface
   */
  protected $contextHandler;

  /**
   * Flag indicating whether the plugin contexts have been initialized.
   *
   * @var bool
   */
  protected $initializedDefinedContexts;

  /**
   * {@inheritdoc}
   */
  public function getContextValue($name) {
    if (!$this->initializedDefinedContexts) {
      $this->initializedDefinedContexts = TRUE;
      $this->initializeDefinedContextValues();
    }
    return $this->parentGetContextValue($name);
  }

  /**
   * Initializes values for the defined contexts of this plugin.
   *
   * @throws \Drupal\Component\Plugin\Exception\ContextException
   */
  protected function initializeDefinedContextValues() {
    $available_contexts = $this->contextRepository->getAvailableContexts();
    $available_runtime_contexts = $this->contextRepository->getRuntimeContexts(array_keys($available_contexts));
    $plugin_context_definitions = $this->getContextDefinitions();
    foreach ($plugin_context_definitions as $name => $plugin_context_definition) {
      $matches = $this->contextHandler->getMatchingContexts($available_runtime_contexts, $plugin_context_definition);
      $matching_context = reset($matches);
      $this->setContextValue($name, $matching_context->getContextValue());
    }
  }

}
