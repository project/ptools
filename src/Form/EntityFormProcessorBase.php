<?php

namespace Drupal\ptools\Form;

use Drupal\Core\Entity\ContentEntityFormInterface;
use Drupal\Core\Entity\Display\EntityFormDisplayInterface;
use Drupal\Core\Entity\EntityFormInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Base class for form processors.
 */
abstract class EntityFormProcessorBase extends FormProcessorBase {

  /**
   * {@inheritdoc}
   */
  public function alter(array &$form, FormStateInterface $form_state, string $form_id = NULL): void {
    parent::alter($form, $form_state, $form_id);

    $this->addEntityBuilder($form);
  }

  /**
   * Returns the entity form object.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\Core\Entity\EntityFormInterface
   *   An entity form object.
   */
  protected function getEntityFormObject(FormStateInterface $form_state): EntityFormInterface {
    $form_object = $form_state->getFormObject();
    assert($form_object instanceof EntityFormInterface);
    return $form_object;
  }

  /**
   * Returns the entity form display, if available.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\Core\Entity\Display\EntityFormDisplayInterface|null
   *   The form display or NULL if not available.
   */
  protected function getEntityFormDisplay(FormStateInterface $form_state): ?EntityFormDisplayInterface {
    $form_object = $form_state->getFormObject();
    return $form_object instanceof ContentEntityFormInterface ?
      $form_object->getFormDisplay($form_state) : NULL;
  }

  /**
   * Returns the form entity, if this is an entity form.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   An entity object.
   */
  protected function getEntity(FormStateInterface $form_state): EntityInterface {
    return $this->getEntityFormObject($form_state)
      ->getEntity();
  }

  /**
   * Entity builder callback.
   *
   * @param string $entity_type_id
   *   The form entity type ID.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The form entity.
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function buildEntity(string $entity_type_id, EntityInterface $entity, array &$form, FormStateInterface $form_state): void {
  }

  /**
   * Adds the specified method as an entity builder.
   *
   * @param array $form
   *   The form array.
   * @param string $method
   *    (optional) The method name. Defaults to "buildEntity".
   */
  protected function addEntityBuilder(array &$form, string $method = 'buildEntity'): void {
    $form['#entity_builders'][] = $this->getEntityBuilderCallback($method);
  }

  /**
   * Returns an entity builder callback invoking the specified processor method.
   *
   * @param string $method
   *   The method name.
   *
   * @return array
   *   A callable array representing the specified entity builder callback.
   */
  protected function getEntityBuilderCallback(string $method): array {
    // This is safe to serialize.
    return [new EntityBuilderCallback(static::class, $method), 'invoke'];
  }

}
