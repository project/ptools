<?php

namespace Drupal\ptools\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\ptools\Render\CallbackBase;

/**
 * Class representing a serializable form callback.
 */
class FormCallback extends CallbackBase {

  /**
   * Invokes the form processor method.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface|null $form_state
   *   The form state.
   * @param array|null $complete_form
   *   The complete form, in case this is a form element callback.
   *
   * @return array|null
   *   The processed form/element or NULL if no return value is expected.
   */
  public function invoke(array &$form, FormStateInterface $form_state = NULL, array $complete_form = NULL): ?array {
    return $this->getInstance()->{$this->method}($form, $form_state, $complete_form);
  }

}
