<?php

namespace Drupal\ptools\Form;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\ptools\Render\CallbackBase;

/**
 * Class representing a serializable entity builder form callback.
 */
class EntityBuilderCallback extends CallbackBase {

  /**
   * Invokes the form processor method.
   *
   * @param string $entity_type_id
   *   The form entity type ID.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The form entity.
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface|null $form_state
   *   The form state.
   */
  public function invoke(string $entity_type_id, EntityInterface $entity, array $form, FormStateInterface $form_state): void {
    $this->getInstance()->{$this->method}($entity_type_id, $entity, $form, $form_state);
  }

}
