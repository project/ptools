<?php

namespace Drupal\ptools\Form;

use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\EventSubscriber\MainContentViewSubscriber;
use Drupal\Core\Form\FormBuilderInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Security\TrustedCallbackInterface;
use Drupal\ptools\Render\PreRenderCallback;
use Drupal\ptools\SingletonTrait;
use Symfony\Component\HttpFoundation\Request;

/**
 * Base class for form processors.
 */
abstract class FormProcessorBase implements FormProcessorInterface, TrustedCallbackInterface {

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected MessengerInterface $messenger;

  /**
   * @see SingletonTrait::get()
   */
  abstract public static function get();

  /**
   * {@inheritdoc}
   */
  public static function trustedCallbacks() {
    return ['preRender'];
  }

  /**
   * {@inheritdoc}
   */
  public function alter(array &$form, FormStateInterface $form_state, string $form_id = NULL): void {
    $this->addProcessCallback($form);
    $this->addPreRenderCallback($form);
    $this->addValidationHandler($form);
    $this->addSubmitHandler($form);
  }

  /**
   * {@inheritdoc}
   */
  public function preRender(array $form): array {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function process(array $form, FormStateInterface $form_state): array {
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validate(array &$form, FormStateInterface $form_state): void {
  }

  /**
   * {@inheritdoc}
   */
  public function submit(array &$form, FormStateInterface $form_state): void {
  }

  /**
   * Adds a process callback to the specified element.
   *
   * @param array $element
   *   A form element.
   * @param string $method
   *   (optional) A method name.
   */
  protected function addProcessCallback(array &$element, string $method = 'process') {
    $element['#process'][] = $this->getCallback($method);
  }

  /**
   * Adds a pre-render callback to the specified element.
   *
   * @param array $element
   *   A form element.
   * @param string $method
   *   (optional) A method name.
   */
  protected function addPreRenderCallback(array &$element, string $method = 'preRender') {
    $element['#pre_render'][] = $this->getCallback($method);
  }

  /**
   * Adds a validation handler to the specified element.
   *
   * @param array $element
   *   A form element.
   * @param string $method
   *   (optional) A method name.
   */
  protected function addValidationHandler(array &$element, string $method = 'validate') {
    $element['#validate'][] = $this->getCallback($method);
  }

  /**
   * Adds a validation handler to the specified form action element.
   *
   * @param array $form
   *   The form array.
   * @param string $method
   *   (optional) A method name.
   */
  protected function addActionValidationHandler(array &$form, string $action = 'submit', string $method = 'validate') {
    $this->addValidationHandler($form['actions'][$action], $method);
  }

  /**
   * Adds a submit handler to the specified element.
   *
   * @param array $element
   *   A form element.
   * @param string $method
   *   (optional) A method name.
   */
  protected function addSubmitHandler(array &$element, string $method = 'submit') {
    $element['#submit'][] = $this->getCallback($method);
  }

  /**
   * Adds a submit handler to the specified form action element.
   *
   * @param array $form
   *   The form array.
   * @param string $method
   *   (optional) A method name.
   */
  protected function addActionSubmitHandler(array &$form, string $action = 'submit', string $method = 'submit') {
    $this->addSubmitHandler($form['actions'][$action], $method);
  }

  /**
   * Returns a form callback invoking the specified processor method.
   *
   * @param string $method
   *   A method name.
   *
   * @return array
   *   A callable array representing the specified form callback.
   */
  protected function getCallback(string $method): array {
    // This is safe to serialize.
    return [new FormCallback(static::class, $method), 'invoke'];
  }

  /**
   * Returns a pre-render callback invoking the specified processor method.
   *
   * @param string $method
   *   (optional) A method name. Defaults to "preRender".
   *
   * @return array
   *   A callable array representing the specified pre-render callback.
   */
  protected function getPreRenderCallback(string $method = 'preRender'): array {
    // This is safe to serialize.
    return [new PreRenderCallback(static::class, $method), 'invoke'];
  }

  /**
   * Builds the $form['#action'].
   *
   * @return string
   *   The URL to be used as the $form['#action'].
   *
   * @see \Drupal\Core\Form\FormBuilder::buildFormAction()
   */
  protected function buildFormAction(Request $request): string {
    $request_uri = $request->getRequestUri();

    // Prevent cross site requests via the Form API by using an absolute URL
    // when the request uri starts with multiple slashes..
    if (strpos($request_uri, '//') === 0) {
      $request_uri = $request->getUri();
    }

    // @todo Remove this parsing once these are removed from the request in
    //   https://www.drupal.org/node/2504709.
    $parsed = UrlHelper::parse($request_uri);
    unset($parsed['query'][FormBuilderInterface::AJAX_FORM_REQUEST], $parsed['query'][MainContentViewSubscriber::WRAPPER_FORMAT]);
    $action = $parsed['path'] . ($parsed['query'] ? ('?' . UrlHelper::buildQuery($parsed['query'])) : '');
    return UrlHelper::filterBadProtocol($action);
  }

}
