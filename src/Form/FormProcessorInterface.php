<?php

namespace Drupal\ptools\Form;

use Drupal\Core\Form\FormStateInterface;

/**
 * Common interface for form processors.
 */
interface FormProcessorInterface {

  /**
   * Implements hook_form_FORM_ID_alter().
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   * @param string|null $form_id
   *   (optional) The form identifier. Defaults to none.
   */
  public function alter(array &$form, FormStateInterface $form_state, string $form_id = NULL): void;

  /**
   * Process callback.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The processed form array.
   */
  public function process(array $form, FormStateInterface $form_state): array;

  /**
   * Pre-render callback.
   *
   * @param array $form
   *   The form array.
   *
   * @return array
   *   The processed form array.
   */
  public function preRender(array $form): array;

  /**
   * Validation handler.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function validate(array &$form, FormStateInterface $form_state): void;

  /**
   * Submit handler.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function submit(array &$form, FormStateInterface $form_state): void;

}
