<?php

namespace Drupal\ptools\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Base class for GET forms.
 */
abstract class GetFormBase extends FormBase {

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form['#method'] = 'GET';
    $form['#process'][] = [$this, 'processForm'];
    return $form;
  }

  /**
   * Process callback.
   */
  public function processForm(array $form): array {
    unset(
      $form['form_build_id'],
      $form['form_token'],
      $form['form_id']
    );
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // This is a GET form, nothing to do here.
  }

}
