<?php

namespace Drupal\ptools\Utils;

/**
 * Array-related utility API.
 */
class Arr {

  /**
   * Inserts a new new array elements after the key in the array.
   *
   * @param string $key
   *   The key to insert after.
   * @param $array
   *   An array to insert in to.
   * @param $data
   *   An array of the data to be inserted.
   *
   * @return array
   *   The new array.
   */
  public static function insertAfter($key, array $array, array $data): array {
    $offset = array_search($key, array_keys($array));
    // Check if the key doesn't exist.
    if ($offset === false) {
      // Set the data as the last element.
      $offset = count($array);
    } else {
      $offset++;
    }

    return self::insertAtPosition($offset, $array, $data);
  }

  /**
   * Inserts a new array elements before the key in the array.
   *
   * @param string $key
   *   The key to insert before.
   * @param $array
   *   An array to insert in to.
   * @param $data
   *   An array of the data to be inserted.
   *
   * @return array
   *   The new array.
   */
  public static function insertBefore($key, array $array, array $data): array {
    $offset = array_search($key, array_keys($array));
    // Check if the key doesn't exist.
    if ($offset === false) {
      // Set the data as the last element.
      $offset = 0;
    }

    return self::insertAtPosition($offset, $array, $data);
  }

  /**
   * Inserts a new array elements at a specific position in the array.
   *
   * @param int $offset
   *   The offset for slice.
   * @param $array
   *   An array to insert in to.
   * @param $data
   *   An array of the data to be inserted.
   *
   * @return array
   *   The new array.
   */
  public static function insertAtPosition(int $offset, array $array, array $data): array {
    return array_slice($array, 0, $offset, true) +
      $data +
      array_slice($array, $offset, count($array) - 1, true);
  }

  /**
   * Flattens a multidimensional array.
   *
   * @param array[] $array
   *   A multidimensional array.
   *
   * @return array
   *   An array of non-array values.
   */
  public static function flatten(array $array) {
    $return = [];
    array_walk_recursive($array, static function ($value) use (&$return) { $return[] = $value; });
    return $return;
  }

}
