<?php

namespace Drupal\ptools\Utils;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Site\Settings;
use Drupal\ptools\CurrentUserTrait;
use Drupal\ptools\SingletonTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class collecting date-related helper methods.
 */
class DateUtils implements ContainerInjectionInterface {

  use CurrentUserTrait;
  use SingletonTrait;

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The service configuration.
   *
   * @var array
   */
  protected $config;

  /**
   * DateUtils constructor.
   */
  public function __construct(
    TimeInterface $time,
    ConfigFactoryInterface $config_factory,
    AccountInterface $current_user,
    array $config
  ) {
    $this->configFactory = $config_factory;
    $this->time = $time;
    $this->currentUser = $current_user;
    $this->config = $config + ['offset' => NULL];
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('datetime.time'),
      $container->get('config.factory'),
      $container->get('current_user'),
      [
        'offset' => Settings::get('ptools.date.offset'),
      ]
    );
  }

  /**
   * Returns the specified date time.
   *
   * @param string $time
   *   (optional) The date time to be represented. Defaults to the current time.
   * @param string|null $timezone
   *   (optional) A timezone identifier. Defaults to the current timezone.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime
   *   A datetime object.
   */
  public function getDateTime(string $time = 'now', string $timezone = NULL): DrupalDateTime {
    $date = $this->getCurrentDateTime($timezone);

    // Set the requested date/time.
    if ($time !== 'now') {
      $date->modify($time);
    }

    return $date;
  }

  /**
   * Returns the current date time.
   *
   * @param string|null $timezone
   *   (optional) A timezone identifier. Defaults to the current timezone.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime
   *   A datetime object.
   */
  public function getCurrentDateTime(string $timezone = NULL): DrupalDateTime {
    // Set the current timezone, without affecting the current time.
    $timestamp = $this->time->getCurrentTime();
    $date = new DrupalDateTime('now', $timezone ?? $this->getCurrentTimezone());
    $date->setTimestamp($timestamp);

    // Alter the current date/time, if needed.
    if ($this->config['offset']) {
      if (is_numeric($this->config['offset'])) {
        $date->setTimestamp($timestamp + $this->config['offset']);
      }
      else {
        $timezone = $date->getTimezone();
        $offset_date = new DrupalDateTime($this->config['offset'], $timezone);
        $date->setTimezone($offset_date->getTimezone());
        $date->modify($this->config['offset']);
        $date->setTimezone($timezone);
      }
    }

    return $date;
  }

  /**
   * Returns a date/time object for the specified timestamp.
   *
   * @param int|null $timestamp
   *   (optional) A Unix timestamp. Defaults to the current request time.
   * @param string|null $timezone
   *   (optional) A timezone identifier. Defaults to none.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime
   *   A date/time object.
   */
  public function dateFromTimestamp(int $timestamp = NULL, string $timezone = NULL): DrupalDateTime {
    $date = $this->getCurrentDateTime($timezone);
    $date->setTimestamp($timestamp ?? $this->time->getRequestTime());
    return $date;
  }

  /**
   * Returns the week start timestamp for the specified time.
   *
   * @param string $time
   *   A PHP time.
   * @param string|null $timezone
   *   (optional) A timezone identifier. Defaults to none.
   *
   * @return int
   *   A Unix timestamp.
   */
  public function getWeekStartTimestamp(string $time, string $timezone = NULL): int {
    $date = $this->getCurrentDateTime($timezone);
    if ($time) {
      $date->modify($time);
    }
    $week_start_date = $this->getWeekStart($date);
    return $week_start_date->getTimestamp();
  }

  /**
   * Returns the week start for the specified time.
   *
   * @param \Drupal\Core\Datetime\DrupalDateTime|null $date
   *   (optional) A date/time object. Defaults to one representing the current
   *   time.
   *
   * @return \Drupal\Core\Datetime\DrupalDateTime
   *   A date/time object representing the week start.
   */
  public function getWeekStart(DrupalDateTime $date = NULL): DrupalDateTime {
    if (!isset($date)) {
      $date = $this->getCurrentDateTime();
    }
    $first_day = (int) $this->configFactory->get('system.date')->get('first_day');
    $week_day = (int) $date->format('w');
    $week_number = (int) $date->format('W');
    // ISO-8601 week number week in the year considers weeks starting on Monday,
    // if the first day is Sunday the "next" week should be selected.
    if ($first_day < 1 && $week_day === $first_day) {
      $week_number++;
    }
    $date->setISODate($date->format('Y'), $week_number, $first_day);
    $date->setTime(0, 0);
    return $date;
  }

  /**
   * Returns the current timezone.
   *
   * @return string
   *   A timezone identifier.
   */
  public function getCurrentTimezone(): string {
    return $this->getUserTimezone($this->currentUser);
  }

  /**
   * Returns the current timezone.
   *
   * @param \Drupal\Core\Session\AccountInterface $account
   *   A user account object.
   *
   * @return string
   *   A timezone identifier.
   */
  public function getUserTimezone(AccountInterface $account): string {
    $config = $this->configFactory->get('system.date');
    if ($config->get('timezone.user.configurable') && $account->isAuthenticated() && $account->getTimezone()) {
      $timezone = $account->getTimeZone();
    }
    else {
      $timezone = $this->getDefaultTimezone();
    }
    return $timezone;
  }

  /**
   * Returns the default timezone.
   *
   * @return string
   *   A timezone identifier.
   */
  public function getDefaultTimezone(): string {
    $config = $this->configFactory->get('system.date');
    $default_timezone = $config->get('timezone.default');
    return $default_timezone ?: date_default_timezone_get();
  }

}
