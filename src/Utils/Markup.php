<?php

namespace Drupal\ptools\Utils;

use Drupal\Component\Render\PlainTextOutput;

/**
 * Class collecting HTML markup-related helper methods.
 */
class Markup {

  /**
   * Checks whether the specified string is HTML markup.
   *
   * @param string $markup
   *   An arbitrary string.
   *
   * @return bool
   *   TRUE if the string is HTML markup, FALSE otherwise.
   */
  public static function isPlainText($markup) {
    $plain_text = PlainTextOutput::renderFromHtml($markup);
    return strlen($markup) == strlen($plain_text);
  }

}
