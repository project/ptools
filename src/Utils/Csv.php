<?php

namespace Drupal\ptools\Utils;

use Drupal\Core\Site\Settings;

/**
 * CSV utility API.
 */
class Csv {

  /**
   * Guesses the delimiter of a CSV file.
   *
   * Works by trying to parse the first lines of a CSV file with known
   * delimiters.
   *
   * @param string $path
   *   The path to the file to analyze.
   * @param int|null $lines_no
   *   (optional) The number of lines to analyze. Defaults to the value of the
   *   "ptools_csv_delimiter_guess_lines" variable, which by default is 30.
   *
   * @return string
   *   The delimiter, or FALSE if none was found.
   */
  public static function guessDelimiter(string $path, int $lines_no = NULL): string {
    if (is_null($lines_no)) {
      $lines_no = Settings::get('ptools_csv_delimiter_guess_lines', 30);
    }
    ini_set('auto_detect_line_endings',TRUE);
    if (!$handle = fopen($path, "r")) {
      return FALSE;
    }
    $candidates = [];
    $delimiters = Settings::get('ptools_csv_delimiter_guesses', [',', ';', "\t"]);
    foreach ($delimiters as $delimiter) {
      $handle = fopen($path, "r");
      $counters = [];
      foreach (range(1, $lines_no) as $delta) {
        $result = TRUE;
        // Filter out empty files.
        $data = fgetcsv($handle, 0, $delimiter);
        if (!$data) {
          // End of file.
          break;
        }
        // Skip empty lines.
        elseif (array_filter($data)) {
          $count = count($data);
          if ($count == 1) {
            // Just one field means the line was not parsed.
            $result = FALSE;
            break;
          }
          $counters[] = $count;
          if (min($counters) != max($counters)) {
            // If the number of fields is not always the same, something is
            // wrong.
            $result = FALSE;
          }
        }
      }
      if ($result && !empty($counters)) {
        $candidates[] = $delimiter;
      }
      fclose($handle);
    }

    if (count($candidates) != 1) {
      throw new \LogicException(sprintf('Csv::guessDelimiter(): zero or more than one CSV delimiter found for file "%s". Either increase the "ptools_csv_delimiter_guess_lines" variable, or specify the delimiter to the parsing function.', $path));
    }
    else {
      return reset($candidates);
    }
  }

  /**
   * Converts a comma separated file into an associative array.
   *
   * The first row should contain the array keys.
   *
   * @param string $path
   *   Path to the CSV file.
   * @param string|null $delimiter
   *   (optional) The field delimiter character passed to fgetcsv(). If NULL, it
   *   will be guessed. Defaults to NULL.
   * @param string $enclosure
   *   (optional) The field enclosure character passed to fgetcsv(). Defaults to
   *   double quotes.
   * @param bool $trim
   *   (optional) Whether to trim() each field of every record. Defaults to
   *   FALSE.
   * @param bool $clean
   *   (optional) Whether to remove any non-ascii character from all data.
   *   Defaults to FALSE.
   * @param array $skip_rows
   *   (optional) The rows to skip; starts from 1, not 0. Defaults to an empty
   *   array.
   *
   * @return array|bool
   *   The associative array built from the file, or FALSE if an error occurred.
   */
  public static function toArray(string $path, string $delimiter = NULL, string $enclosure = '"', bool $trim = FALSE, bool $clean = FALSE, array $skip_rows = []) {
    ini_set('auto_detect_line_endings',TRUE);
    if (!file_exists($path) || !is_readable($path)) {
      return FALSE;
    }

    if (is_null($delimiter)) {
      $delimiter = static::guessDelimiter($path);
    }
    $header = NULL;
    $data = [];
    if (($handle = fopen($path, 'r')) !== FALSE) {
      $count = 0;
      while (($row = fgetcsv($handle, 0, $delimiter, $enclosure)) !== FALSE) {
        $count++;
        if (in_array($count, $skip_rows) || !array_filter($row)) {
          // Skip lines to be skipped, and empty lines.
          continue;
        }
        $row = Text::utf8Encode($row);
        if ($trim) {
          $row = array_map('trim', $row);
        }
        if ($clean) {
          $row = preg_replace('/[^[:ascii:]]/', '', $row);
        }
        if (!$header) {
          $header = $row;
        }
        else {
          if (count($header) > count($row)) {
            $difference = count($header) - count($row);
            for ($i = 1; $i <= $difference; $i++) {
              $row[count($row) + 1] = $delimiter;
            }
          }
          // Combine header and values, eventually removing the empty key.
          $data[] = array_diff_key(array_combine($header, $row), array_flip(['']));
        }
      }
      fclose($handle);
    }
    return $data;
  }

}
