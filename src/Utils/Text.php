<?php

namespace Drupal\ptools\Utils;

use Drupal\Component\Utility\Unicode;

/**
 * Text-related utility API.
 */
class Text {

  /**
   * Returns the UTF-16 value of a character.
   *
   * This is the unicode/multibyte equivalent of ord().
   *
   * @param string $u
   *   The character to convert.
   *
   * @return int
   *   Its UTF-16 decimal value.
   */
  public static function uniord(string $u): int {
    $k = mb_convert_encoding($u, 'UCS-2LE', 'UTF-8');
    $k1 = ord(substr($k, 0, 1));
    $k2 = ord(substr($k, 1, 1));
    return $k2 * 256 + $k1;
  }

  /**
   * Returns a valid machine name from a human readable name.
   *
   * @param string $human_name
   *   A human readable string.
   *
   * @return string
   *   The corresponding machine name.
   *
   * @see http://www.makina-corpus.org/blog/code-snippet-human-readable-machine-name
   */
  public static function toMachineName(string $human_name): string {
    $patterns = [
      '/[^a-zA-Z0-9]+/',
      '/-+/',
      '/^-+/',
      '/-+$/',
    ];
    return mb_strtolower(preg_replace($patterns, ['_', '_', '', ''], $human_name));
  }

  /**
   * Decodes a string from machine name.
   *
   * This implements a best-effort strategy to convert a machine name to a
   * plausible human-readable name, but cannot guarantee full complementarity
   * with ::toMachineName().
   *
   * @param string $string
   *   A machine-name string.
   *
   * @return string
   *   A human readable string.
   */
  public static function fromMachineName(string $string): string {
    return Unicode::ucfirst(str_replace('_', ' ', $string));
  }

  /**
   * Converts a "snake_case" string to "CamelCase".
   *
   * @param string $snake_case_string
   *   A "snake_case" string.
   *
   * @return string
   *   The corresponding "CamelCase" string.
   */
  public static function toCamelCase(string $snake_case_string): string {
    return str_replace('_', '', Unicode::ucwords($snake_case_string));
  }

  /**
   * Converts a "CamelCase" string to "snake_case".
   *
   * @param string $camel_case_string
   *   A "CamelCase" string.
   *
   * @return string
   *   The corresponding "snake_case" string.
   *
   * @see https://stackoverflow.com/questions/1993721/how-to-convert-pascalcase-to-pascal-case
   */
  public static function toSnakeCase(string $camel_case_string): string {
    preg_match_all('!([A-Z][A-Z0-9]*(?=$|[A-Z][a-z0-9])|[A-Za-z][a-z0-9]+)!', $camel_case_string, $matches);
    $ret = $matches[0];
    foreach ($ret as &$match) {
      $match = $match === mb_strtoupper($match) ? mb_strtolower($match) : lcfirst($match);
    }
    return implode('_', $ret);
  }

  /**
   * Converts ISO-8859-1 data into UTF-8, if needed.
   *
   * @param string|array $input
   *   The input data, which can be a string or an array of strings.
   *
   * @return string|array
   *   The data eventually converted to UTF-8.
   */
  public static function utf8Encode($input) {
    if (is_array($input)) {
      return array_map(__METHOD__, $input);
    }
    elseif (is_string($input)) {
      if (!function_exists('mb_detect_encoding')) {
        throw new \LogicException('mbstring extension is required by ' . __METHOD__  . '.');
      }
      // Strict detection (third param) is required.
      $encoding = mb_detect_encoding($input, 'UTF-8', TRUE);
      if ($encoding != 'UTF-8') {
        if (empty($encoding)) {
          // Assume ISO-8859-1 if mb_detect_encoding() didn't work.
          $encoding = 'ISO-8859-1';
        }
        return Unicode::convertToUtf8($input, $encoding);
      }
      else {
        return $input;
      }
    }
    return NULL;
  }

}
