<?php

namespace Drupal\ptools\Utils;

use Drupal\Component\Utility\UrlHelper;

/**
 * Url-related utility API.
 */
class Url {

  /**
   * Reverses UrlHelper::parse().
   *
   * @param array $parsed_url
   *   The output from the parse method.
   *
   * @return string
   *   The constructed URL.
   */
  public static function buildParsedUrlPath(array $parsed_url): string {
    $path = $parsed_url['path'] ?? '';
    if ($parsed_url['query']) {
      $path .= '?' . UrlHelper::buildQuery($parsed_url['query']);
    }
    if ($parsed_url['fragment']) {
      $path .= '#' . $parsed_url['fragment'];
    }
    return $path;
  }

}
