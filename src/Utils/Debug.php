<?php

namespace Drupal\ptools\Utils;

/**
 * Miscellaneous debug-related utility methods.
 */
class Debug {

  /**
   * Returns a formatted backtrace.
   *
   * @return string
   *   A formatted backtrace, similar to the output of debug_print_backtrace().
   */
  public static function getFormattedBacktrace(): string {
    $i = 0;
    $backtrace = array_reduce(debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS), function ($carry, $item) use (&$i) {
      $entry = sprintf('%s()', $item['function'] ?? '');
      if (isset($item['class'])) {
        $entry = sprintf('%s%s%s', $item['class'], $item['type'], $entry);
      }
      if (isset($item['file']) && isset($item['line'])) {
        $entry = sprintf('%s called at [%s:%d]', $entry, $item['file'], $item['line']);
      }
      $entry = sprintf("#%d %s\n", $i++, $entry);
      return $carry . $entry;
    });
    return $backtrace;
  }

  /**
   * Find an object matching the specified class in a data structure.
   *
   * @param mixed $item
   *   An arbitrary data structure.
   * @param string[] $parents
   *   An array of array key or object property names.
   * @param string $class
   *   The class name to be matched.
   *
   * @return array|null
   *   An array of keys useful to identify the object or NULL if it could not be
   *   found.
   */
  public static function findObject($item, array $parents, string $class): ?array {
    $result = NULL;

    if (is_array($item)) {
      foreach ($item as $key => $value) {
        $item_parents = $parents;
        $item_parents[] = $key;
        $result = static::findObject($value, $item_parents, $class);
        if ($result) {
          break;
        }
      }
    }
    elseif (is_object($item)) {
      $result = static::findObjectDeep($item, [], $class);
      if ($result) {
        return array_merge($parents, ['::'], $result);
      }
    }

    return $result;
  }

  /**
   * Find an object matching the specified class nested into a parent object.
   *
   * @param object $object
   *   An object to traverse.
   * @param string[] $parents
   *   An array of array key or object property names.
   * @param string $class
   *   The class name to be matched.
   *
   * @return array|null
   *   An array of keys useful to identify the object or NULL if it could not be
   *   found.
   */
  public static function findObjectDeep(object $object, array $parents, string $class): ?array {
    static $seen_objects;

    if (is_a($object, $class)) {
      return $parents;
    }

    // Prevent circular dependencies from causing infinite loops.
    if (!isset($seen_objects)) {
      $seen_objects = new \SplObjectStorage();
    }
    if (!empty($seen_objects[$object])) {
      return NULL;
    }
    else {
      $seen_objects[$object] = TRUE;
    }

    $reflector = new \ReflectionClass($object);
    foreach ($reflector->getProperties() as $property) {
      try {
        $property->setAccessible(TRUE);
        $real_value = $property->getValue($object);
        $values = is_array($real_value) ? $real_value : [$real_value];

        foreach ($values as $key => $value) {
          if (is_object($value)) {
            $value_parents = $parents;
            $value_parents[] = $property->getName();
            if (is_array($real_value)) {
              $value_parents[] = $key;
            }

            $result = static::findObjectDeep($value, $value_parents, $class);

            if (isset($result)) {
              $seen_objects = NULL;
              return $result;
            }
          }
        }
      }
      catch (\Throwable $t) {
      }
    }

    return NULL;
  }

}
