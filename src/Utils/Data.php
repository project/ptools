<?php

namespace Drupal\ptools\Utils;

use Drupal\Core\Entity\FieldableEntityInterface;

/**
 * Miscellaneous data structure-related utility methods.
 */
class Data {

  /**
   * Returns a property value for each element of the specified array.
   *
   * @param string $property
   *   The name of the property that should be retrieved from every array
   *   element.
   * @param array[]|object[] $data
   *   An array of associative arrays or objects.
   *
   * @return array
   *   An array of property values.
   */
  public static function pluck(string $property, array $data): array {
    return array_map(
      function ($item) use ($property) {
        assert(is_array($item) || is_object($item));
        if (is_array($item)) {
          return $item[$property] ?? NULL;
        }
        elseif (is_object($item)) {
          if ($item instanceof FieldableEntityInterface && $item->hasField($property)) {
            $value = $item->get($property)->getValue();
            return current(current($value));
          }
          elseif (method_exists($item, $property)) {
            return $item->{$property}();
          }
          elseif (property_exists($item, $property)) {
            return $item->{$property};
          }
        }
        return NULL;
      },
      $data
    );
  }

  /**
   * Returns a property value for each element of the specified array.
   *
   * @param string $property
   *   The name of the property that should be retrieved from every array
   *   element.
   * @param array[]|object[] $data
   *   An array of associative arrays or objects.
   *
   * @return array
   *   An associative array of property values keyed with the same property
   *   values.
   */
  public static function pluckCombine(string $property, array $data): array {
    $values = static::pluck($property, $data);
    return array_combine($values, $values);
  }

}
