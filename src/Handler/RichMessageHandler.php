<?php

declare(strict_types=1);

namespace Drupal\ptools\Handler;

use Drupal\Core\KeyValueStore\KeyValueFactoryInterface;
use Drupal\Core\KeyValueStore\KeyValueStoreInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\ptools\CurrentUserTrait;

/**
 * User notification service for rich message contents.
 *
 * @private
 */
class RichMessageHandler {

  use CurrentUserTrait;

  protected const COLLECTION = 'ptools.rich_message_handler';

  /**
   * Key value store.
   *
   * @var \Drupal\Core\KeyValueStore\KeyValueStoreInterface
   */
  protected KeyValueStoreInterface $keyValueStore;

  /**
   * Constructs a RichMessageHandler object.
   */
  public function __construct(KeyValueFactoryInterface $key_value_factory, AccountInterface $current_user) {
    $this->keyValueStore = $key_value_factory->get(static::COLLECTION);
    $this->currentUser = $current_user;
  }

  /**
   * Stores a new message for the user.
   *
   * @param array $message
   *   The message render array.
   * @param \Drupal\Core\Session\AccountInterface|null $account
   *   (optional) The recipient. Defaults to the current user.
   * @param bool $prioritize
   *   If TRUE, it'll be returned as the first message in the queue.
   */
  public function addMessage(array $message, AccountInterface $account = NULL, bool $prioritize = FALSE): void {
    $key = $this->getAccount($account)->id();
    $existing = $this->keyValueStore->get($key) ?? [];
    if ($prioritize) {
      array_unshift($existing, $message);
    }
    else {
      $existing[] = $message;
    }
    $this->keyValueStore->set($key, $existing);
  }

  /**
   * Returns all stored messages.
   *
   * @param \Drupal\Core\Session\AccountInterface|null $account
   *   (optional) The recipient. Defaults to the current user.
   * @param bool $clear
   *   TRUE if the read messages should be cleared from storage.
   *
   * @return array
   *   The array of messages.
   */
  public function getAll(AccountInterface $account = NULL, bool $clear = TRUE): array {
    // @todo add a session-based cache here.
    $key = $this->getAccount($account)->id();
    $existing = $this->keyValueStore->get($key) ?? [];
    if ($clear) {
      $this->keyValueStore->set($key, []);
    }
    return $existing;
  }

  /**
   * Returns a stored message.
   *
   * @param \Drupal\Core\Session\AccountInterface|null $account
   *   (optional) The recipient. Defaults to the current user.
   * @param bool $clear
   *   TRUE if the read message should be cleared from storage.
   *
   * @return array|null
   *   NULL if there are no messages. Array for the message render array.
   */
  public function getOne(AccountInterface $account = NULL, bool $clear = TRUE): ?array {
    $key = $this->getAccount($account)->id();
    $existing = $this->keyValueStore->get($key);
    if (!$existing) {
      return NULL;
    }
    $first = array_shift($existing);
    if ($clear) {
      $this->keyValueStore->set($key, $existing);
    }
    return $first;
  }

}
