<?php

namespace Drupal\ptools\Handler;

use Drupal\Core\Cache\CacheableResponseInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup as t;
use Drupal\Core\Url;
use Drupal\ptools\ExceptionLoggerTrait;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Sub-requests handler.
 */
class SubrequestHandler {

  use ExceptionLoggerTrait;

  /**
   * The HTTP kernel.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected HttpKernelInterface $httpKernel;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected RequestStack $requestStack;

  /**
   * The messenger service.
   *
   * @var \Drupal\Core\Messenger\MessengerInterface
   */
  protected MessengerInterface $messenger;

  /**
   * SubrequestControllerBase constructor.
   */
  public function __construct(
    HttpKernelInterface $http_kernel,
    RequestStack $request_stack,
    LoggerInterface $logger_channel,
    MessengerInterface $messenger
  ) {
    $this->httpKernel = $http_kernel;
    $this->requestStack = $request_stack;
    $this->loggerChannel = $logger_channel;
    $this->messenger = $messenger;
  }

  /**
   * Performs a sub-request to the specified URL.
   *
   * @param \Drupal\Core\Url $url
   *   The target URL.
   * @param array $attributes
   *   (optional) An array of request attributes. Defaults to none.
   * @param array $query
   *   (optional) An array of query parameters.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   The HTTP response.
   *
   * @throws \Exception
   */
  public function doSubrequest(Url $url, array $attributes = [], array $query = []): Response {
    // Skip cookie authentication check on sub-requests.
    unset($query['check_logged_in']);
    $cacheable_url = $url->toString(TRUE);
    $sub_request = Request::create($cacheable_url->getGeneratedUrl(), 'GET', $query);
    if ($attributes) {
      $sub_request->attributes->add($attributes);
    }
    $main_request = $this->requestStack->getMainRequest();
    if ($main_request->hasSession()) {
      $sub_request->setSession($main_request->getSession());
    }
    $response = $this->httpKernel->handle($sub_request, HttpKernelInterface::SUB_REQUEST);
    if ($response instanceof CacheableResponseInterface) {
      $response->addCacheableDependency($cacheable_url);
    }
    return $response;
  }

  /**
   * Performs a sub-request to the specified URL.
   *
   * @param \Drupal\Core\Url $url
   *   The target URL.
   * @param array $attributes
   *   (optional) An array of request attributes. Defaults to none.
   * @param array $query
   *   (optional) An array of query parameters.
   *
   * @return \Symfony\Component\HttpFoundation\Response|null
   *   The HTTP response or NULL if an error occurred.
   */
  public function doSubrequestWithExceptionHandling(Url $url, array $attributes = [], array $query = []): ?Response {
    try {
      return $this->doSubrequest($url, $attributes, $query);
    }
    catch (\Exception $e) {
      $this->logException($e);
      $this->messenger->addError(new t('An error occurred, please try again later.'));
      return NULL;
    }
  }

  /**
   * Returns the main request.
   *
   * @return \Symfony\Component\HttpFoundation\Request
   *   an HTTP request object.
   */
  public function getMainRequest(): Request {
    return $this->requestStack->getMainRequest();
  }

}
