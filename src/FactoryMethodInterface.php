<?php

namespace Drupal\ptools;

/**
 * Common interface for classes implementing a factory method.
 */
interface FactoryMethodInterface {

  /**
   * Returns an instance of the implementing class.
   *
   * @param array $context
   *   (optional) An associative array of additional dependencies. Defaults to
   *   none.
   *
   * @return static
   *   An object instance.
   */
  public static function create(array $context = []);

}
