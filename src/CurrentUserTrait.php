<?php

namespace Drupal\ptools;

use Drupal\Core\Session\AccountInterface;
use Drupal\ptools_entity\BasicEntityFieldHandlerTrait;
use Drupal\user\UserInterface;

/**
 * Trait useful to retrieve a valid user account.
 */
trait CurrentUserTrait {

  use BasicEntityFieldHandlerTrait;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * Returns a valid account, falling back to the current one.
   *
   * @param \Drupal\Core\Session\AccountInterface|null $account
   *   (optional) A user account.
   *
   * @return \Drupal\Core\Session\AccountInterface
   *   A user account instance.
   */
  protected function getAccount(AccountInterface $account = NULL): AccountInterface {
    return $account ?? $this->currentUser;
  }

  /**
   * Returns the user entity matching the specified account.
   *
   * @param \Drupal\Core\Session\AccountInterface|int|null $account
   *   (optional) A user account object or ID. Defaults to NULL.
   *
   * @return \Drupal\user\UserInterface|null
   *   A user entity object or NULL if none could be found.
   */
  protected function getAccountUser($account = NULL): ?UserInterface {
    $id = is_numeric($account) ? $account : NULL;
    $id = $id ?? $this->getAccount($account instanceof AccountInterface ? $account : NULL)->id();
    $user = $this->getStorage('user')->load($id);
    return $user instanceof UserInterface ? $user : NULL;
  }

}
