<?php

namespace Drupal\ptools;

use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\Utility\Error;
use Psr\Log\LogLevel;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Trait implementing exception logging.
 */
trait ExceptionLoggerTrait {

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $loggerChannel;

  /**
   * Logs the specified exception.
   *
   * @param \Exception $exception
   *   The exception to be logged.
   * @param string $severity
   *   (optional) Log severity.
   *
   * @see \watchdog_exception
   */
  protected function logException(\Exception $exception, $severity = LogLevel::ERROR): void {
    static::doLogException($this->loggerChannel, $exception, $severity);
  }

  /**
   * Actually logs the specified exception.
   *
   * @param \Drupal\Core\Logger\LoggerChannelInterface|string $target
   *   The logger to be used or the channel to log to.
   * @param \Exception $exception
   *   The exception to be logged.
   * @param string $severity
   *   (optional) Log severity.
   *
   * @see \watchdog_exception
   */
  protected static function doLogException($target, \Exception $exception, $severity = LogLevel::ERROR) {
    assert($target instanceof LoggerChannelInterface || is_string($target));
    $logger = $target instanceof LoggerChannelInterface ? $target : \Drupal::logger($target);
    $message = '%type: @message in %function (line %line of %file).';
    $context = Error::decodeException($exception);
    $logger->log($severity, $message, $context);
  }

  /**
   * Retrieves the specified logger channel from the DI container.
   *
   * @param \Symfony\Component\DependencyInjection\ContainerInterface $container
   *   The DI container.
   * @param string $channel
   *   The logger channel name.
   *
   * @return \Drupal\Core\Logger\LoggerChannelInterface
   *   A logger channel.
   */
  protected static function getLoggerChannel(ContainerInterface $container, string $channel): LoggerChannelInterface {
    $logger_channel = $container->get('logger.channel.' . $channel);
    assert($logger_channel instanceof LoggerChannelInterface);
    return $logger_channel;
  }

}
