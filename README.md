PTools
======

This exposes a set of miscellaneous public APIs. Anything marked as `@deprecated`
will be removed before publishing the first stable release.
