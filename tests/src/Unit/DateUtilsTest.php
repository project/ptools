<?php

namespace Drupal\Tests\ptools\Unit;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\DependencyInjection\ContainerBuilder;
use Drupal\Core\DependencyInjection\ContainerNotInitializedException;
use Drupal\Core\Language\Language;
use Drupal\Core\Language\LanguageManagerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\ptools\Utils\DateUtils;
use Drupal\Tests\UnitTestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

/**
 * @coversDefaultClass \Drupal\ptools\Utils\DateUtils
 *
 * @group ptools
 */
class DateUtilsTest extends UnitTestCase {

  use ProphecyTrait;

  const DEFAULT_TIMEZONE = 'America/New_York';

  /**
   * The time service.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * {@inheritdoc}
   *
   * @noinspection PhpUndefinedMethodInspection
   */
  protected function setUp(): void {
    parent::setUp();

    $this->initContainer();

    $time = $this->prophesize(TimeInterface::class);
    $time->getCurrentTime()
      ->willReturn($_SERVER['REQUEST_TIME']);
    $this->time = $time->reveal();

    $this->currentUser = $this->prophesize(AccountInterface::class)
      ->reveal();

    $this->initConfigFactory(0);
  }

  /**
   * Initializes the service container.
   *
   * @noinspection PhpUndefinedMethodInspection
   */
  protected function initContainer(): void {
    try {
      \Drupal::getContainer();
    }
    catch (ContainerNotInitializedException $e) {
      $container = new ContainerBuilder();
      $language_manager = $this->prophesize(LanguageManagerInterface::class);
      $language_manager->getCurrentLanguage()
        ->willReturn(new Language());
      $container->set('language_manager', $language_manager->reveal());
      \Drupal::setContainer($container);
    }
  }

  /**
   * Initializes the config factory.
   *
   * @param int $first_day
   *   The first day of the week to be used.
   *
   * @noinspection PhpUndefinedMethodInspection
   */
  protected function initConfigFactory(int $first_day): void {
    $config_system_date = $this->prophesize(ImmutableConfig::class);
    $config_system_date->get(Argument::any())
      ->will(static function ($args) use ($first_day) {
        $map = [
          'timezone.user.configurable' => FALSE,
          'timezone.default' => static::DEFAULT_TIMEZONE,
          'first_day' => $first_day,
        ];
        return $map[$args[0]] ?? NULL;
      });

    $config_factory = $this->prophesize(ConfigFactoryInterface::class);
    $config_factory->get('system.date')
      ->willReturn($config_system_date->reveal());
    $this->configFactory = $config_factory->reveal();
  }

  /**
   * @covers ::getCurrentDateTime
   * @covers ::getCurrentTimezone
   * @covers ::getDefaultTimezone
   *
   * @dataProvider providerGetCurrentDateTime
   */
  public function testGetCurrentDateTime(array $config, ?string $timezone, int $expected): void {
    $date_utils = new DateUtils($this->time, $this->configFactory, $this->currentUser, $config);
    $current_time = $date_utils->getCurrentDateTime($timezone);
    $this->assertEquals($timezone ?: static::DEFAULT_TIMEZONE, $current_time->getTimezone()->getName());
    $this->assertEquals($expected, $current_time->getTimestamp());
  }

  /**
   * Data provider for "testGetCurrentDateTime"
   */
  public static function providerGetCurrentDateTime(): array {
    return [
      'No offset, no timezone' => [
        ['offset' => NULL], NULL, $_SERVER['REQUEST_TIME'],
      ],
      'No offset, timezone' => [
        ['offset' => NULL], 'UTC', $_SERVER['REQUEST_TIME'],
      ],
      'Numeric offset, no timezone' => [
        ['offset' => -3600], NULL, $_SERVER['REQUEST_TIME'] - 3600,
      ],
      'Numeric offset, timezone' => [
        ['offset' => -3600], 'UTC', $_SERVER['REQUEST_TIME'] - 3600,
      ],
      'Relative time offset, no timezone' => [
        ['offset' => '-1 day -2 hours'], NULL, $_SERVER['REQUEST_TIME'] - ((24 + 2) * 3600),
      ],
      'Relative time offset, timezone' => [
        ['offset' => '-1 day -2 hours'], 'UTC', $_SERVER['REQUEST_TIME'] - ((24 + 2) * 3600),
      ],
      'Absolute time offset, no timezone' => [
        ['offset' => '2022-03-27 05:15 UTC'], NULL, 1648358100,
      ],
      'Absolute time offset, timezone' => [
        ['offset' => '2022-03-27 05:15 UTC'], 'UTC', 1648358100,
      ],
    ];
  }

  /**
   * @covers ::getWeekStartTimestamp
   *
   * @dataProvider providerGetWeekStartTimestamp
   */
  public function testGetWeekStartTimestamp(array $config, string $time, int $expected): void {
    $date_utils = new DateUtils($this->time, $this->configFactory, $this->currentUser, $config);
    $timestamp = $date_utils->getWeekStartTimestamp($time);
    $this->assertEquals($expected, $timestamp);
  }

  /**
   * Data provider for "testGetWeekStartTimestamp"
   */
  public static function providerGetWeekStartTimestamp(): array {
    return [
      'Saturday -> Mar 20' => [
        ['offset' => '2022-03-26 05:00 UTC'], '', 1647748800,
      ],
      'Saturday, New York midnight -> Mar 20' => [
        ['offset' => '2022-03-26 05:00 UTC'], '-1 hour', 1647748800,
      ],
      'Sunday -> Mar 27' => [
        ['offset' => '2022-03-27 05:00 UTC'], '', 1648353600,
      ],
      'Sunday, New York midnight -> Mar 27' => [
        ['offset' => '2022-03-27 05:00 UTC'], '-1 hour', 1648353600,
      ],
      'Monday -> Mar 27' => [
        ['offset' => '2022-03-28 05:00 UTC'], '', 1648353600,
      ],
      'Monday, New York midnight -> Mar 27' => [
        ['offset' => '2022-03-28 05:00 UTC'], '-1 hour', 1648353600,
      ],
    ];
  }

  /**
   * @covers ::getWeekStart
   *
   * @dataProvider providerGetWeekStart
   */
  public function testGetWeekStart(DrupalDateTime $date, int $first_day, int $expected): void {
    $this->initConfigFactory($first_day);
    $date_utils = new DateUtils($this->time, $this->configFactory, $this->currentUser, []);
    $week_start = $date_utils->getWeekStart($date);
    $this->assertEquals($expected, $week_start->getTimestamp());
  }

  /**
   * Data provider for "testGetWeekStart"
   */
  public function providerGetWeekStart(): array {
    $this->initContainer();
    return [
      'Saturday -> Mar 20, First day Sunday' => [
        new DrupalDateTime('2022-03-26 05:00', 'UTC'), 0, 1647734400,
      ],
      'Saturday -> Mar 20, New York timezone, First day Sunday' => [
        new DrupalDateTime('2022-03-26 05:00', static::DEFAULT_TIMEZONE), 0, 1647748800,
      ],
      'Sunday -> Mar 27, First day Sunday' => [
        new DrupalDateTime('2022-03-27 05:00', 'UTC'), 0, 1648339200,
      ],
      'Sunday -> Mar 27, New York timezone, First day Sunday' => [
        new DrupalDateTime('2022-03-27 05:00', static::DEFAULT_TIMEZONE), 0, 1648353600,
      ],
      'Monday -> Mar 27, First day Sunday' => [
        new DrupalDateTime('2022-03-28 05:00','UTC'), 0, 1648339200,
      ],
      'Monday -> Mar 27, New York timezone, First day Sunday' => [
        new DrupalDateTime('2022-03-28 05:00',static::DEFAULT_TIMEZONE), 0, 1648353600,
      ],
      'Sunday -> Mar 21, First day Monday' => [
        new DrupalDateTime('2022-03-27 05:00', 'UTC'), 1, 1647820800,
      ],
      'Sunday -> Mar 21, New York timezone, First day Monday' => [
        new DrupalDateTime('2022-03-27 05:00', static::DEFAULT_TIMEZONE), 1, 1647835200,
      ],
      'Monday -> Mar 28, First day Monday' => [
        new DrupalDateTime('2022-03-28 05:00','UTC'), 1, 1648425600,
      ],
      'Monday -> Mar 28, New York timezone, First day Monday' => [
        new DrupalDateTime('2022-03-28 05:00',static::DEFAULT_TIMEZONE), 1, 1648440000,
      ],
      'Tuesday -> Mar 28, First day Monday' => [
        new DrupalDateTime('2022-03-29 05:00', 'UTC'), 1, 1648425600,
      ],
      'Tuesday -> Mar 28, New York timezone, First day Monday' => [
        new DrupalDateTime('2022-03-29 05:00', static::DEFAULT_TIMEZONE), 1, 1648440000,
      ],
    ];
  }

}
