<?php

namespace Drupal\ptools_queue\Commands;

use Drush\Commands\DrushCommands;

/**
 * Queue handler Drush commands.
 */
class DrushQueueHandler extends DrushCommands {

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The queue handler.
   *
   * @var \Drupal\ptools_queue\Handler\QueueHandlerFactory
   */
  protected $queueHandlerFactory;

  /**
   * QueueHandlerCommands constructor.
   */
  public function __construct($queue_factory, $queue_handler_factory) {
    parent::__construct();

    $this->queueFactory = $queue_factory;
    $this->queueHandlerFactory = $queue_handler_factory;
  }

  /**
   * Queues the specified items for processing.
   *
   * @command ptools-queue-items
   * @description Queues up some items in the specified queues. A comma separated list of items is accepted, if no item is specified all items are queued up.
   * @aliases pt-qi
   * @bootstrap full
   * @option options Additional options to be passed to the queue handler.
   *
   * @param string $queue_name
   *   The queue name.
   * @param string|null $items
   *   (optional) A comma-separated list of items. Defaults to queueing all
   *   pending items.
   */
  public function queueItems(string $queue_name, string $items = NULL, array $options = ['options' => '']) {
    if (is_string($items)) {
      $items = array_map('trim', explode(',', $items));
    }

    $processed_options = [];
    if ($options['options'] && is_string($options['options'])) {
      foreach (array_map('trim', explode(',', $options['options'])) as $option) {
        $parts = explode(':', $option, 2);
        if (count($parts) === 2) {
          [$name, $value] = $parts;
          $processed_options[$name] = $value;
        }
      }
    }

    $this->queueHandlerFactory
      ->getQueueHandler($queue_name)
      ->queueItems($items ?: [], $processed_options);
  }

  /**
   * Clears the specified queue.
   *
   * @command ptools-queue-clear
   * @aliases pt-qc
   * @bootstrap full
   *
   * @param string $queue_name
   *   The queue name.
   */
  public function clearQueue($queue_name) {
    $this->queueHandlerFactory
      ->getQueueHandler($queue_name)
      ->clearQueue();
  }

  /**
   * Processes the specified queue.
   *
   * @command ptools-queue-process
   * @aliases pt-qp pt-qr ptools-queue-run
   * @option limit Limits the processing to the specified amount of items.
   * @option time-limit Limits the processing to the specified amount of seconds.
   * @bootstrap full
   *
   * @param string $queue_name
   *   The queue name.
   */
  public function processQueue($queue_name, array $options = ['limit' => -1, 'time_limit' => NULL]) {
    $this->queueHandlerFactory
      ->getQueueHandler($queue_name)
      ->processItems($options['limit'], $options['time_limit']);
  }

  /**
   * Prints the amount of queued items to "stdout".
   *
   * @command ptools-queue-size
   * @aliases pt-qs
   * @bootstrap full
   *
   * @param string $queue_name
   *   The queue name.
   */
  public function queueSize($queue_name) {
    print $this->queueFactory->get($queue_name)->numberOfItems() . "\n";
  }

}
