<?php

namespace Drupal\ptools_queue;

use Drupal\Core\Queue\QueueWorkerInterface;

/**
 * Common interface for queue workers supporting queue handlers.
 */
interface QueueHandlerWorkerInterface extends QueueWorkerInterface {

  /**
   * Returns the total number of pending items.
   *
   * @return int
   *   The pending items count.
   */
  public function getPendingItemsCount();

  /**
   * Fetches a batch of pending items to be queued.
   *
   * @param int $offset
   *   (optional) The offset index to start from. Defaults to 0.
   *
   * @return array
   *   An array of pending items.
   */
  public function fetchPendingItems($offset = 0);

  /**
   * Notifies the queue worker that the specified items were queued.
   *
   * @param array[] $items
   *   An array of queue items.
   * @param bool $items_specified
   *   (optional) TRUE if the items were explicitly specified, FALSE if all
   *   items were loaded automatically.
   */
  public function queuedItems(array $items, $items_specified = TRUE);

}
