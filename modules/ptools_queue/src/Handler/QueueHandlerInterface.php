<?php

namespace Drupal\ptools_queue\Handler;

/**
 * Common interface for queue handlers.
 */
interface QueueHandlerInterface {

  /**
   * Queues the specified items for processing..
   *
   * @param array|null $items
   *   (optional) An array of item to be processed. Defaults to queueing all
   *   pending items.
   * @param array $options
   *   (optional) An array of options to be forwarded to the queue processor.
   */
  public function queueItems(array $items = [], array $options = []);

  /**
   * Processes the queued items.
   *
   * @param int $limit
   *   (optional) The maximum number of items that should be processed. Defaults
   *   to no limit.
   * @param int $time_limit
   *   (optional) The maximum number of seconds processing should run. Defaults
   *   to no limit.
   *
   * @return int
   *   The number of items actually processed.
   */
  public function processItems($limit = -1, $time_limit = NULL);

  /**
   * Clears the queue.
   */
  public function clearQueue();

}
