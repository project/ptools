<?php

namespace Drupal\ptools_queue\Handler;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\ptools\ExceptionLoggerTrait;

/**
 * The queue handler factory.
 */
class QueueHandlerFactory {

  use ExceptionLoggerTrait;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * The queue worker manager.
   *
   * @var \Drupal\Core\Queue\QueueWorkerManagerInterface
   */
  protected $queueWorkerManager;

  /**
   * The logger channel.
   *
   * @var \Drupal\Core\Logger\LoggerChannelInterface
   */
  protected $loggerChannel;

  /**
   * QueueHandlerFactory constructor.
   */
  public function __construct($queue_factory, $queue_worker_manager, $logger_channel) {
    $this->queueFactory = $queue_factory;
    $this->queueWorkerManager = $queue_worker_manager;
    $this->loggerChannel = $logger_channel;
  }

  /**
   * Returns a queue handler instance.
   *
   * @param string $queue_name
   *   The queue name.
   *
   * @return \Drupal\ptools_queue\Handler\QueueHandlerInterface
   *   A queue handler instance.
   */
  public function getQueueHandler($queue_name) {
    $queue = $this->queueFactory->get($queue_name);
    try {
      $queue_worker = $this->queueWorkerManager->createInstance($queue_name);
      return new QueueHandler($queue, $queue_worker, $this->loggerChannel);
    }
    catch (PluginException $e) {
      $this->logException($e);
    }
    return NULL;
  }

}
