<?php

namespace Drupal\ptools_queue\Handler;

use Drupal\Core\Site\Settings;
use Drupal\ptools\ExceptionLoggerTrait;

/**
 * Base class for queue handler plugins.
 */
class QueueHandler implements QueueHandlerInterface {

  use ExceptionLoggerTrait;

  const DEFAULT_LEASE_TIME = 15;

  /**
   * Shutdown buffer useful to trigger garbage collection in case of OOMs.
   *
   * @var bool
   */
  protected static $shutdownBuffer;

  /**
   * The handled queue.
   *
   * @var \Drupal\Core\Queue\QueueInterface
   */
  protected $queue;

  /**
   * The queue worker.
   *
   * @var \Drupal\ptools_queue\QueueHandlerWorkerInterface
   */
  protected $queueWorker;

  /**
   * The worker definition.
   *
   * @var array
   */
  protected $definition;

  /**
   * The item currently being processed.
   *
   * @var \stdClass
   */
  protected $currentItem;

  /**
   * The item processing start time in microseconds.
   *
   * @var float
   */
  protected $itemStart;

  /**
   * The item processing end time in microseconds.
   *
   * @var float
   */
  protected $itemEnd;

  /**
   * QueueWorkerBase constructor.
   */
  public function __construct($queue, $queue_worker, $logger_channel) {
    $this->queue = $queue;
    $this->queueWorker = $queue_worker;
    $this->loggerChannel = $logger_channel;

    $this->initShutdownBuffer();
  }

  /**
   * Initializes the shutdown buffer.
   *
   * This is used to handle fatal errors (e.g. out of memory errors) gracefully.
   */
  protected function initShutdownBuffer() {
    if (!static::$shutdownBuffer) {
      $size = Settings::get('ptools_queue.shutdown_buffer.size', 1024);
      static::$shutdownBuffer = str_pad('*', $size);
      $self = $this;
      drupal_register_shutdown_function(function () use ($self) {
        if (!$self->currentItem) {
          return;
        }
        // Trigger garbage collection.
        $self::$shutdownBuffer = NULL;
        $error = error_get_last();
        if (isset($error)) {
          $variables = [
            '!message' => $error['message'],
            '!file' => $error['file'],
            '!line' => $error['line'],
          ];
          $self->loggerChannel->error('!message (line !line of !file).', $variables);
          $self->handleItemFailure($self->currentItem);
        }
      });
    }
  }

  /**
   * {@inheritdoc}
   */
  public function queueItems(array $items = [], array $options = []) {
    $items_specified = (bool) $items;

    if ($items_specified) {
      $processed = $this->createItems($items, $options);
    }
    else {
      $processed = 0;
      $total = $this->queueWorker->getPendingItemsCount();
      while ($processed < $total) {
        $items = $this->queueWorker->fetchPendingItems($processed);
        $processed += $this->createItems($items, $options);
        $this->loggerChannel->notice('-> @count / @total processed', ['@count' => $processed, '@total' => $total]);
      }
    }

    if ($processed) {
      $this->queueWorker->queuedItems($items, $items_specified);
    }

    $this->loggerChannel->notice('@count items added to the queue.', ['@count' => $processed]);
  }

  /**
   * Creates the specified queue items.
   *
   * @param array $items
   *   An array of queue items.
   * @param array $options
   *   An array of queue handler-specific options.
   *
   * @return int
   *   The number of processed items.
   */
  protected function createItems(array $items, array $options) {
    $definition = $this->getWorkerDefinition();
    $this->queue->createQueue();
    foreach ($items as $item) {
      $this->queue->createItem([
        'item' => $item,
        'attempts' => $definition['max_attempts'],
        'options' => $options,
      ]);
    }
    return count($items);
  }

  /**
   * {@inheritdoc}
   */
  public function processItems($limit = -1, $time_limit = NULL) {
    $definition = $this->getWorkerDefinition();

    $processed = 0;
    $failed = 0;
    $batch_size = $definition['batch_size'] ?? 100;
    $count = intval($limit) ?: -1;
    if (isset($time_limit)) {
      $end = time() + $time_limit;
    }

    while ($count-- !== 0 && (!isset($end) || time() < $end) && ($item = $this->queue->claimItem($definition['lease_time']))) {
      try {
        $this->handleItemStart($item);
        $this->queueWorker->processItem($item->data);
        $this->handleItemSuccess($item);
        if (++$processed % $batch_size === 0) {
          $this->loggerChannel->notice('@processed_count queue items processed.', ['@processed_count' => $processed]);
        }
      }
      catch (\Exception $e) {
        // In case of exception log it and leave the item in the queue to be
        // processed again later.
        $this->logException($e);
        $this->handleItemFailure($item);
        if ($item->data['attempts'] <= 1) {
          $failed++;
        }
      }
    }

    $args = [
      '@processed_count' => $processed,
      '@failed_count' => $failed,
      '@total' => $this->queue->numberOfItems(),
    ];
    $this->loggerChannel->notice('@processed_count queue items processed, @failed_count failed, @total remaining.', $args);

    return $processed;
  }

  /**
   * Handles item processing start.
   *
   * @param \stdClass $item
   *   A queue item.
   */
  protected function handleItemStart($item) {
    $definition = $this->getWorkerDefinition();
    $this->currentItem = $item;
    // If the queue is expected to be empty at the end of the process, we need
    // to delete the item before processing it, in case the process fails
    // without performing proper clean-up.
    if ($definition['clear_failed']) {
      $this->queue->deleteItem($item);
    }
    $this->itemStart = microtime(TRUE);
  }

  /**
   * Handles item processing success.
   *
   * @param \stdClass $item
   *   A queue item.
   */
  protected function handleItemSuccess($item) {
    $this->itemEnd = microtime(TRUE);
    $this->queue->deleteItem($item);
    $this->currentItem = NULL;
  }

  /**
   * Handles item processing failure.
   *
   * @param \stdClass $item
   *   A queue item.
   */
  protected function handleItemFailure($item) {
    $this->itemEnd = microtime(TRUE);

    // Before giving up, we queue up the item again until we reach the maximum
    // number of attempts.
    if ($item->data['attempts'] > 1) {
      $this->queue->deleteItem($item);
      $item->data['attempts']--;
      $this->queue->createItem($item->data);
    }
    elseif ($item->data['attempts'] !== -1) {
      $args = [
        '@name' => $this->queueWorker->getPluginId(),
        '@item_id' => $item->item_id,
        '@count' => $this->definition['max_attempts'],
        '@item' => print_r($item->data['item'], TRUE),
      ];
      $this->loggerChannel->error('Item "@item_id" of queue "@name" could not be processed after @count attempts: <pre>@item</pre>', $args);
    }

    $this->currentItem = NULL;
  }

  /**
   * {@inheritdoc}
   */
  public function clearQueue() {
    $processed = $this->queue->numberOfItems();
    $this->queue->deleteQueue();
    $this->loggerChannel->notice('@count items removed from queue.', ['@count' => $processed]);
    return $processed;
  }

  /**
   * Returns the queue worker definition.
   *
   * @return array
   *   The queue worker definition.
   */
  protected function getWorkerDefinition() {
    if (!isset($this->definition)) {
      $definition = $this->queueWorker->getPluginDefinition() + [
        'max_attempts' => 1,
        'clear_failed' => FALSE,
      ];
      $definition['lease_time'] = $definition['lease_time'] ?? $definition['cron']['time'] ?? static::DEFAULT_LEASE_TIME;
      $this->definition = $definition;
    }
    return $this->definition;
  }

}
