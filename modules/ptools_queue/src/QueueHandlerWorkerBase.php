<?php

namespace Drupal\ptools_queue;

use Drupal\Core\Queue\QueueWorkerBase;

/**
 * Base class for queue handler plugins.
 */
abstract class QueueHandlerWorkerBase extends QueueWorkerBase implements QueueHandlerWorkerInterface {

  /**
   * {@inheritdoc}
   */
  public function getPendingItemsCount() {
    return 0;
  }

  /**
   * {@inheritdoc}
   */
  public function fetchPendingItems($offset = 0) {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function queuedItems(array $items = NULL, $items_specified = FALSE) {
  }

  /**
   * {@inheritdoc}
   */
  public final function processItem($data) {
    $this->doProcessItem($data['item'] ?? $data, $data['options'] ?? []);
  }

  /**
   * Actually processes the specified data.
   *
   * @param mixed $data
   *   The data that was passed to
   *   \Drupal\Core\Queue\QueueInterface::createItem() when the item was queued.
   */
  abstract protected function doProcessItem($data, array $options = []);

}
