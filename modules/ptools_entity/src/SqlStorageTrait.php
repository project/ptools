<?php

namespace Drupal\ptools_entity;

use Drupal\Core\Entity\Sql\SqlEntityStorageInterface;

/**
 * Trait to retrieve the SQL storage.
 */
trait SqlStorageTrait {

  use EntityFieldHandlerTrait;

  /**
   * Returns the wrapped entity storage handler.
   *
   * @param string $entity_type_id
   *   The ID of the entity type being handled.
   *
   * @return \Drupal\Core\Entity\Sql\SqlEntityStorageInterface
   *   A SQL storage handler using the current database connection.
   */
  protected function getSqlStorage(string $entity_type_id): SqlEntityStorageInterface {
    $storage = $this->getStorage($entity_type_id);
    assert(isset($storage) && $storage instanceof SqlEntityStorageInterface);
    return $storage;
  }

}
