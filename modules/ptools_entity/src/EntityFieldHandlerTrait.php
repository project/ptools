<?php

namespace Drupal\ptools_entity;

/**
 * Trait to retrieve the entity storage.
 */
trait EntityFieldHandlerTrait {

  use BasicEntityFieldHandlerTrait;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The entity file manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

}
