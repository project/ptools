<?php

namespace Drupal\ptools_entity;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\taxonomy\TermStorageInterface;

/**
 * Trait to retrieve the taxonomy storage.
 */
trait TaxonomyTermStorageTrait {

  /**
   * Returns the term storage.
   *
   * @return \Drupal\taxonomy\TermStorageInterface
   *   The taxonomy term storage.
   */
  protected function getTermStorage(): TermStorageInterface {
    try {
      assert(isset($this->entityTypeManager) && $this->entityTypeManager instanceof EntityTypeManagerInterface);
      $storage = $this->entityTypeManager->getStorage('taxonomy_term');
    }
    catch (PluginException) {}
    assert(isset($storage) && $storage instanceof TermStorageInterface);
    return $storage;
  }

}
