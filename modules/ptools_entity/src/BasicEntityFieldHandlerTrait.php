<?php

namespace Drupal\ptools_entity;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;

/**
 * Trait to retrieve the entity storage.
 */
trait BasicEntityFieldHandlerTrait {

  /**
   * Returns the bundle entity storage handler.
   *
   * @param string $entity_type_id
   *   The ID of the entity type being handled.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   A storage handler for the specified entity type.
   */
  protected function getStorage(string $entity_type_id): EntityStorageInterface {
    try {
      assert(isset($this->entityTypeManager) && $this->entityTypeManager instanceof EntityTypeManagerInterface);
      $storage = $this->entityTypeManager->getStorage($entity_type_id);
    }
    catch (PluginException $e) {
    }
    assert(isset($storage) && $storage instanceof EntityStorageInterface);
    return $storage;
  }

  /**
   * Returns the original version of the specified entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   An entity object.
   *
   * @return \Drupal\Core\Entity\EntityInterface|null
   *   The original entity object or NULL if the entity is new.
   */
  protected function getOriginalEntity(EntityInterface $entity): ?EntityInterface {
    if ($entity->isNew()) {
      return NULL;
    }
    return $entity->original ?? $this->getStorage($entity->getEntityTypeId())->loadUnchanged($entity->id());
  }

}
