<?php

namespace Drupal\ptools_entity\Entity;

use Drupal\taxonomy\TermInterface as CoreTermInterface;

/**
 * Common interface for taxonomy term bundle entities.
 */
interface TermInterface extends BundleEntityInterface, CoreTermInterface, FieldableEntityInterface {

  const ENTITY_TYPE_ID = 'taxonomy_term';

  /**
   * Return the term parents.
   *
   * @return static[]
   *   An associative array of parent terms keyed by parent ID.
   */
  public function getParents(): array;

  /**
   * Sets the term parents.
   *
   * @param static[] $parents
   *   An array of parent terms.
   */
  public function setParents(array $parents): void;

}
