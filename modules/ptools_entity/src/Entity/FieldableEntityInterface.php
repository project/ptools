<?php

namespace Drupal\ptools_entity\Entity;

use Drupal\Core\Entity\FieldableEntityInterface as CoreFieldableEntityInterface;

/**
 * Interface extending core fieldable entity functionality.
 */
interface FieldableEntityInterface extends CoreFieldableEntityInterface {

  /**
   * Returns the item ID.
   *
   * @return int|string
   *   The item identifier.
   */
  public function getIdentifier(): int|string;

  /**
   * Marks the entity as changed.
   */
  public function forceChanged(): void;

  /**
   * Checks whether the entity has changes to the specified field.
   *
   * @param string $field_name
   *   The field name.
   *
   * @return bool
   *   TRUE if the entity has field changes, FALSE otherwise.
   */
  public function hasFieldChanges(string $field_name): bool;

}
