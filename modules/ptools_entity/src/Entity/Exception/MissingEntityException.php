<?php

namespace Drupal\ptools_entity\Entity\Exception;

use Drupal\Core\Entity\EntityStorageException;

/**
 * Thrown when an entity ID does not correspond to a stored entity.
 */
class MissingEntityException extends EntityStorageException {

  /**
   * InvalidEntityIdException constructor.
   *
   * @param string $entity_type_id
   *   An entity typed ID.
   * @param string $id
   *   An entity ID.
   */
  public function __construct(string $entity_type_id, string $id) {
    $message = 'The entity of type "%s" with id "%s" is missing.';
    parent::__construct(sprintf($message, $entity_type_id, $id));
  }

}
