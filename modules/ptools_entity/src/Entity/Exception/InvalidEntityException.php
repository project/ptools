<?php

namespace Drupal\ptools_entity\Entity\Exception;

use Drupal\Core\Entity\EntityInterface;

/**
 * Thrown when trying to instantiate an invalid entity wrapper.
 *
 * @deprecated
 */
class InvalidEntityException extends \InvalidArgumentException {

  /**
   * InvalidEntityException constructor.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   An entity object.
   * @param string $wrapper_class
   *   The wrapper class.
   */
  public function __construct(EntityInterface $entity, string $wrapper_class) {
    $ids = [
      $entity->getEntityTypeId(),
      $entity->bundle(),
      $entity->uuid(),
      $entity->id(),
    ];
    $message = 'The "%s" entity (%s) cannot be wrapped by the "%s" wrapper.';
    parent::__construct(sprintf($message, $entity->label(), implode(':', $ids), $wrapper_class));
  }

}
