<?php

namespace Drupal\ptools_entity\Entity\Exception;

use Drupal\Core\Entity\EntityStorageException;

/**
 * Thrown when an entity ID is not valid.
 */
class InvalidEntityIdException extends EntityStorageException {

  /**
   * InvalidEntityIdException constructor.
   *
   * @param string $entity_type_id
   *   An entity type ID.
   * @param string|null $id
   *   (optional) An entity ID.
   */
  public function __construct(string $entity_type_id, string $id = NULL) {
    $message = 'Invalid ID for entity of type "%s" ("%s").';
    parent::__construct(sprintf($message, $entity_type_id, $id));
  }

}
