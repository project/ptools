<?php

namespace Drupal\ptools_entity\Entity;

use Drupal\group\Entity\Group;

/**
 * Base class for group bundle entities.
 */
abstract class GroupBundleEntityBase extends Group implements GroupInterface, LegacyEntityWrapperInterface {

  use FieldableBundleEntityTrait;
  use LegacyEntityWrapperTrait;

}
