<?php

namespace Drupal\ptools_entity\Entity;

use Drupal\taxonomy\Entity\Term;

/**
 * Base class for taxonomy term bundle entities.
 */
abstract class TermBundleEntityBase extends Term implements TermInterface, LegacyEntityWrapperInterface {

  use FieldableBundleEntityTrait;
  use LegacyEntityWrapperTrait;

  /**
   * {@inheritDoc}
   */
  public function getName() {
    // The parent class returns the label, which feels backwards.
    return (string) $this->getFieldValue('name');
  }

  /**
   * {@inheritdoc}
   */
  public function getParents(): array {
    return $this->getKeyedReferences('parent');
  }

  /**
   * {@inheritdoc}
   */
  public function setParents(array $parents): void {
    $this->setFieldValue('parent', $parents);
  }

}
