<?php

namespace Drupal\ptools_entity\Entity;

use Drupal\media\MediaInterface as CoreMediaInterface;

/**
 * Common interface for node bundle entities.
 */
interface MediaInterface extends BundleEntityInterface, CoreMediaInterface, FieldableEntityInterface {

  const ENTITY_TYPE_ID = 'media';

}
