<?php

namespace Drupal\ptools_entity\Entity;

use Drupal\group\Entity\GroupInterface as BaseGroupInterface;

/**
 * Common interface for group bundle entities.
 */
interface GroupInterface extends BaseGroupInterface, BundleEntityInterface, FieldableEntityInterface {

  const ENTITY_TYPE_ID = 'group';

}
