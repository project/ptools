<?php

namespace Drupal\ptools_entity\Entity;

use Drupal\Core\Entity\EntityInterface;

/**
 * Common interface for bundle entities.
 */
interface BundleEntityInterface extends EntityInterface {

  /**
   * Returns the entity type of the bundle entity.
   *
   * @return string
   *   An entity type ID.
   */
  public static function getBaseEntityTypeId(): string;

}
