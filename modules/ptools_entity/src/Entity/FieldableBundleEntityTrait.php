<?php

namespace Drupal\ptools_entity\Entity;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Config\Entity\ConfigEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\TypedData\Exception\MissingDataException;
use Drupal\Core\TypedData\Exception\ReadOnlyException;

/**
 * Trait extending core fieldable entity functionality.
 *
 * Implements "\Drupal\ptools_entity\Entity\FieldableEntityInterface" and
 * provides additional helper methods.
 *
 * @see \Drupal\ptools_entity\Entity\BundleEntityInterface
 * @see \Drupal\ptools_entity\Entity\FieldableEntityInterface
 */
trait FieldableBundleEntityTrait {

  /**
   * @see \Drupal\ptools_entity\Entity\FieldableEntityInterface::getBaseEntityTypeId()
   */
  public static function getBaseEntityTypeId(): string {
    return static::ENTITY_TYPE_ID;
  }

  /**
   * {@inheritdoc}
   */
  public function getIdentifier(): int|string {
    assert($this instanceof FieldableEntityInterface);
    $id = $this->id();
    return is_numeric($id) ? (int) $id : (string) $id;
  }

  /**
   * Returns a field value.
   *
   * @param string $field_name
   *   The field name.
   * @param string $property_name
   *   (optional) The name of the field property to retrieve. Defaults to
   *   "value".
   *
   * @return mixed
   *   The field value if available, NULL otherwise.
   */
  protected function getFieldValue(string $field_name, string $property_name = 'value'): mixed {
    assert($this instanceof FieldableEntityInterface);
    $value = NULL;
    try {
      $item = $this->get($field_name)->get(0);
      if ($item) {
        $value = $item->getValue()[$property_name] ?? NULL;
      }
    }
    catch (MissingDataException) {
    }
    return $value;
  }

  /**
   * Returns all the field values.
   *
   * @param string $field_name
   *   The field name.
   * @param string $property_name
   *   (optional) The name of the field property to retrieve. Defaults to
   *   "value".
   *
   * @return array
   *   an array of field values.
   */
  protected function getFieldValues(string $field_name, string $property_name = 'value'): array {
    assert($this instanceof FieldableEntityInterface);
    $values = [];
    try {
      foreach ($this->get($field_name) as $item) {
        if ($item) {
          $values[] = $item->getValue()[$property_name] ?? NULL;
        }
      }
    }
    catch (MissingDataException) {
    }
    return $values;
  }

  /**
   * Sets a field value.
   *
   * @param string $field_name
   *   The field name.
   * @param mixed $value
   *   The field value.
   */
  protected function setFieldValue(string $field_name, mixed $value): void {
    assert($this instanceof FieldableEntityInterface);
    $this->set($field_name, $value);
  }

  /**
   * @see \Drupal\ptools_entity\Entity\FieldableEntityInterface::hasFieldChanges()
   */
  public function hasFieldChanges(string $field_name): bool {
    assert($this instanceof FieldableEntityInterface);

    if ($this->isNew()) {
      return FALSE;
    }

    if (!isset($this->original)) {
      $storage = $this->getStorage($this->getEntityTypeId());
      $this->original = $storage->loadUnchanged($this->getIdentifier());
      assert($this->original instanceof FieldableEntityInterface);
    }
    $original = $this->original;

    return !$original->get($field_name)->equals($this->get($field_name));
  }

  /**
   * {@inheritdoc}
   */
  public function forceChanged(): void {
    assert($this instanceof EntityChangedInterface);
    $this->setChangedTime(0);
  }

  /**
   * Returns the values of the specified referenced entity property.
   *
   * @param string $field_name
   *   The entity reference field name.
   * @param string $property
   *   The name of the referenced entity property to be retrieved.
   *
   * @return array
   *   An array of property values.
   */
  protected function getEntityReferenceProperties(string $field_name, string $property): array {
    $values = [];
    $entities = $this->resolveEntityReferences($field_name);
    foreach ($entities as $entity) {
      assert($entity instanceof FieldableEntityInterface || $entity instanceof ConfigEntityInterface);
      $value = $entity->get($property);
      if ($value instanceof FieldItemListInterface) {
        foreach ($value->getValue() as $item_value) {
          $values[] = current($item_value);
        }
      }
      else {
        $values[] = $value;
      }
    }
    return $values;
  }

  /**
   * Returns an array of referenced entities keyed by entity ID.
   *
   * @param string $field_name
   *   The name of the entity reference field.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   An associative array of referenced entities keyed by entity ID and sorted
   *   by item delta.
   */
  protected function getKeyedReferences(string $field_name): array {
    $references = $this->resolveEntityReferences($field_name);
    $ids = array_map(
      function (EntityInterface $entity) {
        return $entity instanceof FieldableEntityInterface ? $entity->getIdentifier() : $entity->id();
      },
      $references
    );
    return array_combine($ids, $references);
  }

  /**
   * Resolves an entity reference field with multiple cardinality.
   *
   * @param string $field_name
   *   The entity reference field name.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   An array of referenced entity.
   */
  protected function resolveEntityReferences(string $field_name): array {
    assert($this instanceof FieldableEntityInterface);
    $field_item_list = $this->get($field_name);
    $entity_type_id = $field_item_list->getSetting('target_type');
    $values = $field_item_list->getValue();
    // Applying array_filter removes null values from the array.
    $ids = array_filter(array_map(function (array $value) { return $value['target_id']; }, $values));
    if (empty($ids)) {
      return [];
    }
    return array_filter($this->getStorage($entity_type_id)->loadMultiple($ids));
  }

  /**
   * Adds referenced item.
   *
   * @param string $field_name
   *   The field name.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   */
  protected function addReferencedItem(string $field_name, EntityInterface $entity): void {
    assert($this instanceof FieldableEntityInterface);
    if (!$this->hasReferencedItem($field_name, $entity)) {
      $this->get($field_name)->appendItem($entity);
    }
  }

  /**
   * Checks whether the given entity is a referenced item.
   *
   * @param string $field_name
   *   The field name.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return bool
   *    TRUE if the entity is referenced, FALSE otherwise.
   */
  protected function hasReferencedItem(string $field_name, EntityInterface $entity): bool {
    return $this->getReferencedItemIndex($field_name, $entity) >= 0;
  }


  /**
   * Get the index of the given entity for the field.
   *
   * @param string $field_name
   *   The field name.
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity.
   *
   * @return int
   *   The index offset, or "-1" if the entity is not present.
   */
  protected function getReferencedItemIndex(string $field_name, EntityInterface $entity): int {
    assert($this instanceof FieldableEntityInterface);
    $values = $this->get($field_name)->getValue();
    $item_ids = array_map(static function ($value) {
      return $value['target_id'];
    }, $values);
    $index = array_search($entity->id(), $item_ids);
    return $index !== FALSE ? (int) $index : -1;
  }

  /**
   * Resolves a field reference with multiple cardinality.
   *
   * @param string $field_name
   *   The entity reference field name.
   */
  protected function setEntityReferencesByProperty(string $field_name, array $values): void {
    assert($this instanceof FieldableEntityInterface);
    $field_item_list = $this->get($field_name);
    $query = NULL;

    foreach ($values as $name => $value) {
      if ($value) {
        if (!isset($query)) {
          $entity_type_id = $field_item_list->getSetting('target_type');
          $query = $this->getStorage($entity_type_id)->getQuery();
          $query->accessCheck(FALSE);
        }

        // Cast scalars to array so we can consistently use an IN condition.
        $query->condition($name, (array) $value, 'IN');
      }
    }

    $items = $query ? array_values($query->execute()) : [];

    try {
      $field_item_list->setValue($items);
    }
    catch (ReadOnlyException) {}
  }

  /**
   * Returns the specified referenced entities.
   *
   * @param string $entity_type_id
   *   The entity type ID.
   * @param array $ids
   *   An array of entity IDs.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   An array of entities.
   */
  protected function getReferencedItemsById(string $entity_type_id, array $ids): array {
    return $this->getStorage($entity_type_id)->loadMultiple($ids);
  }

  /**
   * Returns the storage handler for the specified entity type.
   *
   * @param string $entity_type_id
   *   An entity type ID.
   *
   * @return \Drupal\Core\Entity\EntityStorageInterface
   *   A storage handler instance.
   */
  private function getStorage(string $entity_type_id): EntityStorageInterface {
    try {
      $storage = \Drupal::entityTypeManager()->getStorage($entity_type_id);
    }
    catch (PluginException) {}
    assert(isset($storage) && $storage instanceof EntityStorageInterface);
    return $storage;
  }

}
