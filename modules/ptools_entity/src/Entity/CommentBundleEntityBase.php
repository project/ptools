<?php

namespace Drupal\ptools_entity\Entity;

use Drupal\comment\Entity\Comment;

/**
 * Base class for comment bundle entities.
 */
abstract class CommentBundleEntityBase extends Comment implements CommentInterface, LegacyEntityWrapperInterface {

  use FieldableBundleEntityTrait;
  use LegacyEntityWrapperTrait;

  const FIELD_BODY = 'comment_body';
  const FIELD_SUBJECT = 'subject';

  /**
   * {@inheritdoc}
   */
  public function getBody(): string {
    return $this->getFieldValue(static::FIELD_BODY) ?: '';
  }

  /**
   * {@inheritdoc}
   */
  public function setBody(string $value): void {
    $this->setFieldValue(static::FIELD_BODY, $value);
  }

}
