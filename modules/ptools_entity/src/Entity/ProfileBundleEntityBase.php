<?php

namespace Drupal\ptools_entity\Entity;

use Drupal\profile\Entity\Profile;

/**
 * Base class for profile bundle entities.
 */
abstract class ProfileBundleEntityBase extends Profile implements ProfileInterface, LegacyEntityWrapperInterface {

  use FieldableBundleEntityTrait;
  use LegacyEntityWrapperTrait;

}
