<?php

namespace Drupal\ptools_entity\Entity;

use Drupal\comment\CommentInterface as CoreCommentInterface;

/**
 * Common interface for comment bundle entities.
 */
interface CommentInterface extends BundleEntityInterface, CoreCommentInterface, FieldableEntityInterface {

  const ENTITY_TYPE_ID = 'comment';

  /**
   * Returns the comment body.
   *
   * @return string
   *   The comment body.
   */
  public function getBody(): string;

  /**
   * Sets the comment body.
   *
   * @param string $value
   *   The comment body.
   */
  public function setBody(string $value): void;

}
