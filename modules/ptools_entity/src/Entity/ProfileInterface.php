<?php

namespace Drupal\ptools_entity\Entity;

use Drupal\profile\Entity\ProfileInterface as ContribProfileInterface;

/**
 * Common interface for node bundle entities.
 */
interface ProfileInterface extends BundleEntityInterface, ContribProfileInterface, FieldableEntityInterface {

  const ENTITY_TYPE_ID = 'profile';

}
