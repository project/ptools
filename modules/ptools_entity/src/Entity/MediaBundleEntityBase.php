<?php

namespace Drupal\ptools_entity\Entity;

use Drupal\media\Entity\Media;

/**
 * Base class for media bundle entities.
 */
abstract class MediaBundleEntityBase extends Media implements MediaInterface, LegacyEntityWrapperInterface {

  use FieldableBundleEntityTrait;
  use LegacyEntityWrapperTrait;

}
