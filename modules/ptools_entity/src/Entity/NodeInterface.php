<?php

namespace Drupal\ptools_entity\Entity;

use Drupal\node\NodeInterface as CoreNodeInterface;

/**
 * Common interface for node bundle entities.
 */
interface NodeInterface extends BundleEntityInterface, CoreNodeInterface, FieldableEntityInterface {

  const ENTITY_TYPE_ID = 'node';

}
