<?php

namespace Drupal\ptools_entity\Entity;

use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;

/**
 * Common interface for legacy entity wrappers.
 */
interface LegacyEntityWrapperInterface {

  /**
   * Gets the wrapped entity.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The wrapped entity.
   *
   * @deprecated
   */
  public function getEntity(): EntityInterface;

  /**
   * Gets the wrapped entity.
   *
   * @return \Drupal\Core\Entity\FieldableEntityInterface
   *   The wrapped entity.
   *
   * @deprecated
   */
  public function getFieldableEntity(): FieldableEntityInterface;

  /**
   * Returns the bundle ID of the wrapped entity.
   *
   * @return string
   *   A bundle ID.
   *
   * @deprecated
   */
  public function getBundle(): string;

  /**
   * Checks whether the specified wrapper is wrapping the same entity.
   *
   * @param \Drupal\ptools_entity\Entity\LegacyEntityWrapperInterface $entityWrapper
   *   An entity wrapper.
   *
   * @return bool
   *   TRUE if the wrapped entities are the same, FALSE otherwise. Only the
   *   entity identifier is taken into account.
   *
   * @deprecated
   */
  public function isSame(LegacyEntityWrapperInterface $entityWrapper): bool;

  /**
   * Wraps the first entity referenced by the field name.
   *
   * @param string $field_name
   *   The name of the entity reference field.
   *
   * @return \Drupal\ptools_entity\Entity\LegacyEntityWrapperInterface|null
   *   The wrapped referenced entity.
   *
   * @deprecated
   */
  public function wrapReference(string $field_name): ?LegacyEntityWrapperInterface;

  /**
   * Returns an array of wrapped referenced entities.
   *
   * @param string $field_name
   *   The entity reference field name.
   *
   * @return \Drupal\ptools_entity\Entity\LegacyEntityWrapperInterface[]
   *   An array of referenced entities.
   *
   * @deprecated
   */
  public function wrapReferences(string $field_name): array;

  /**
   * Returns the wrapped comment.
   *
   * @return \Drupal\comment\CommentInterface
   *   A comment entity object.
   *
   * @deprecated
   */
  public function getComment(): \Drupal\comment\CommentInterface;

  /**
   * Returns the wrapped group.
   *
   * @return \Drupal\group\Entity\GroupInterface
   *   A group entity object.
   *
   * @deprecated
   */
  public function getGroup(): \Drupal\group\Entity\GroupInterface;

  /**
   * Returns the wrapped group.
   *
   * @return \Drupal\node\NodeInterface
   *   A node entity object.
   *
   * @deprecated
   */
  public function getNode(): \Drupal\node\NodeInterface;

  /**
   * Returns the wrapped term.
   *
   * @return \Drupal\taxonomy\TermInterface
   *   A taxonomy term object.
   *
   * @deprecated
   */
  public function getTerm(): \Drupal\taxonomy\TermInterface;

}
