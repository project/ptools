<?php

namespace Drupal\ptools_entity\Entity;

use Drupal\node\Entity\Node;

/**
 * Base class for node bundle entities.
 */
abstract class NodeBundleEntityBase extends Node implements NodeInterface, LegacyEntityWrapperInterface {

  use FieldableBundleEntityTrait;
  use LegacyEntityWrapperTrait;

}
