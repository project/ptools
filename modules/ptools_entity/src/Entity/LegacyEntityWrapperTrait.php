<?php

namespace Drupal\ptools_entity\Entity;

use Drupal\comment\CommentInterface;
use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\node\NodeInterface;
use Drupal\ptools_entity\Entity\Exception\InvalidEntityException;
use Drupal\ptools_entity\Entity\Exception\MissingEntityException;
use Drupal\ptools_entity\Repository\LegacyEntityWrapperRepositoryInterface;
use Drupal\ptools_entity\Repository\LegacyEntityWrapperRepositoryTrait;

/**
 * @see \Drupal\ptools_entity\Entity\LegacyEntityWrapperInterface
 *
 * BC layer allowing to migrate from wrapped entities to bundle entities.
 */
trait LegacyEntityWrapperTrait {

  use FieldableBundleEntityTrait;

  /**
   * Legacy reference to itself as the wrapped entity.
   *
   * @deprecated
   */
  protected LegacyEntityWrapperInterface $entity;

  /**
   * Returns a wrapped entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to be wrapped.
   *
   * @return static
   *   A wrapped entity.
   *
   * @throws \Drupal\ptools_entity\Entity\Exception\InvalidEntityException
   *   If the entity cannot be wrapped by the current wrapper.
   *
   * @deprecated
   */
  final public static function wrap(EntityInterface $entity): static {
    if ($entity instanceof LegacyEntityWrapperInterface && is_a($entity, static::class)) {
      /** @noinspection PhpPossiblePolymorphicInvocationInspection */
      $entity->entity = $entity;
      /** @var \Drupal\ptools_entity\Entity\LegacyEntityWrapperTrait $entity */
      return $entity;
    }
    throw new InvalidEntityException($entity, static::class);
  }

  /**
   * Returns a wrapped entity, if a wrapper is available.
   *
   * @param \Drupal\Core\Entity\EntityInterface $entity
   *   The entity to be wrapped.
   *
   * @return static|null
   *   A wrapped entity or NULL if no wrapper is available.
   *
   * @deprecated
   */
  final public static function wrapValid(EntityInterface $entity): ?LegacyEntityWrapperInterface {
    try {
      return static::wrap($entity);
    }
    catch (InvalidEntityException) {
      return NULL;
    }
  }

  /**
   * Returns a wrapped entity.
   *
   * @param string $id
   *   The entity ID.
   *
   * @return static
   *   A wrapped entity.
   *
   * @throws \Drupal\ptools_entity\Entity\Exception\MissingEntityException
   *   If the entity could not be loaded.
   *
   * @deprecated
   */
  final public static function wrapId(string $id): static {
    assert(is_subclass_of(static::class, LegacyEntityWrapperInterface::class));
    try {
      $entity = \Drupal::entityTypeManager()
        ->getStorage(static::getBaseEntityTypeId())
        ->load($id);
    }
    catch (PluginException) {
    }
    if (!isset($entity)) {
      $exception = new \Exception();
      \Drupal::logger('ptools')->error('Can not wrap entity @type with ID @id. <pre>@backtrace</pre>', [
        '@type' => static::getBaseEntityTypeId(),
        '@id' => $id,
        '@backtrace' => $exception->getTraceAsString(),
      ]);
      throw new MissingEntityException(static::getBaseEntityTypeId(), $id);
    }
    return static::wrap($entity);
  }

  /**
   * Returns the legacy repository manager.
   *
   * @return \Drupal\ptools_entity\Repository\LegacyEntityWrapperRepositoryInterface
   *   The legacy repository manager.
   *
   * @deprecated
   */
  final protected function repositoryManager(): LegacyEntityWrapperRepositoryInterface {
    return new class implements LegacyEntityWrapperRepositoryInterface {
      use LegacyEntityWrapperRepositoryTrait;
    };
  }

  /**
   * @see \Drupal\ptools_entity\Entity\LegacyEntityWrapperInterface::getEntity()
   *
   * @deprecated
   */
  final public function getEntity(): EntityInterface {
    assert($this instanceof LegacyEntityWrapperInterface && $this instanceof EntityInterface);
    return $this;
  }

  /**
   * @see \Drupal\ptools_entity\Entity\LegacyEntityWrapperInterface::getFieldableEntity()
   *
   * @deprecated
   */
  final public function getFieldableEntity(): FieldableEntityInterface {
    assert($this instanceof LegacyEntityWrapperInterface);
    assert($this instanceof FieldableEntityInterface);
    return $this;
  }

  /**
   * @see \Drupal\ptools_entity\Entity\LegacyEntityWrapperInterface::getBundle()
   *
   * @deprecated
   */
  final public function getBundle(): string {
    assert($this instanceof EntityInterface);
    return $this->bundle();
  }

  /**
   * @see \Drupal\ptools_entity\Entity\LegacyEntityWrapperInterface::isSame()
   *
   * @deprecated
   */
  final public function isSame(LegacyEntityWrapperInterface $entity_wrapper): bool {
    assert($entity_wrapper instanceof \Drupal\ptools_entity\Entity\FieldableEntityInterface);
    assert($this instanceof EntityInterface);
    assert($this instanceof LegacyEntityWrapperInterface);
    $id = $this->getIdentifier();
    $other_id = $entity_wrapper->getIdentifier();
    return $id === $other_id && $this->getEntityTypeId() === $entity_wrapper->getEntityTypeId();
  }

  /**
   * @see \Drupal\ptools_entity\Entity\LegacyEntityWrapperInterface::wrapReference()
   *
   * @deprecated
   */
  final public function wrapReference(string $field_name): ?LegacyEntityWrapperInterface {
    return current($this->wrapReferences($field_name)) ?: NULL;
  }

  /**
   * @see \Drupal\ptools_entity\Entity\LegacyEntityWrapperInterface::wrapReferences()
   *
   * @deprecated
   */
  final public function wrapReferences(string $field_name): array {
    assert(is_subclass_of($this, \Drupal\ptools_entity\Entity\FieldableEntityInterface::class));
    $entities = $this->resolveEntityReferences($field_name);
    return $this->repositoryManager()->wrapMultiple($entities);
  }

  /**
   * Sets the referenced entities.
   *
   * @param string $field_name
   *   The entity reference field name.
   * @param static[] $entities
   *   An array of $entities or entity wrappers.
   *
   * @deprecated
   */
  final protected function setWrappedReferences(string $field_name, array $entities): void {
    assert(is_subclass_of($this, \Drupal\ptools_entity\Entity\FieldableEntityInterface::class));
    $this->setFieldValue($field_name, $entities);
  }

  /**
   * Returns an array of wrapped references keyed by entity ID.
   *
   * @param string $field_name
   *   The name of the entity reference field.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   An associative array of referenced entities keyed by entity ID and sorted
   *   by item delta.
   *
   * @deprecated
   */
  final protected function getKeyedWrappedReferences(string $field_name): array {
    assert(is_subclass_of($this, \Drupal\ptools_entity\Entity\FieldableEntityInterface::class));
    return $this->getKeyedReferences($field_name);
  }

  /**
   * @see \Drupal\ptools_entity\Entity\LegacyEntityWrapperInterface::getComment()
   *
   * @deprecated   */
  final public function getComment(): CommentInterface {
    assert($this instanceof LegacyEntityWrapperInterface);
    assert($this instanceof CommentInterface);
    return $this;
  }

  /**
   * @see \Drupal\ptools_entity\Entity\LegacyEntityWrapperInterface::getGroup()
   *
   * @deprecated
   */
  final public function getGroup(): GroupInterface {
    assert($this instanceof LegacyEntityWrapperInterface);
    assert($this instanceof GroupInterface);
    return $this;
  }

  /**
   * @see \Drupal\ptools_entity\Entity\LegacyEntityWrapperInterface::getNode()
   *
   * @deprecated
   */
  final public function getNode(): NodeInterface {
    assert($this instanceof LegacyEntityWrapperInterface);
    assert($this instanceof NodeInterface);
    return $this;
  }

  /**
   * @see \Drupal\ptools_entity\Entity\LegacyEntityWrapperInterface::getTerm()
   *
   * @deprecated
   */
  final public function getTerm(): TermInterface {
    assert($this instanceof LegacyEntityWrapperInterface);
    assert($this instanceof TermInterface);
    return $this;
  }

}
