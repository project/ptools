<?php declare(strict_types = 1);

namespace Drupal\ptools_entity\Repository;

use Drupal\block_content\BlockContentInterface;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\State\StateInterface;
use Drupal\ptools_entity\EntityFieldHandlerTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Custom block repository.
 */
class CustomBlockRepository implements ContainerInjectionInterface {

  use EntityFieldHandlerTrait;

  protected const PREFIX = 'ptools.';

  /**
   * The state service.
   *
   * @var \Drupal\Core\State\StateInterface
   */
  protected StateInterface $state;

  /**
   * CustomBlockRepository constructor.
   */
  public function __construct(StateInterface $state, EntityTypeManagerInterface $entity_type_manager) {
    $this->state = $state;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('state'),
      $container->get('entity_type.manager')
    );
  }

  /**
   * Adds a block to the repository.
   *
   * @param string $id
   *   The block ID.
   * @param array $values
   *   The block values.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function addBlock(string $id, array $values): void {
    // Create the block.
    $storage = $this->getStorage('block_content');
    $block = $storage->create($values);
    if ($block instanceof BlockContentInterface) {
      $storage->save($block);
      $this->state->set(static::PREFIX . $id, $block->uuid());
    }
  }

  /**
   * Returns a custom block.
   *
   * @param string $id
   *   The Block ID.
   *
   * @return array
   *   A renderable array.
   */
  public function getBlock(string $id): array {
    $uuid = $this->state->get(static::PREFIX . $id);
    if (!$uuid) {
      return [];
    }

    $block_content = $this->getStorage('block_content')
      ->loadByProperties(['uuid' => $uuid]);
    $block_content = reset($block_content);

    if ($block_content instanceof BlockContentInterface) {
      return $this->entityTypeManager
        ->getViewBuilder('block_content')
        ->view($block_content);
    }

    return [];
  }

}
