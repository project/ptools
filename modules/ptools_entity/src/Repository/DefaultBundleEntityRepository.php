<?php

declare(strict_types=1);

namespace Drupal\ptools_entity\Repository;

/**
 * Default bundle entity repository.
 */
final class DefaultBundleEntityRepository extends BundleEntityRepositoryBase {
}
