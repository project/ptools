<?php

namespace Drupal\ptools_entity\Repository;

use Drupal\Core\Entity\EntityInterface;
use Drupal\ptools_entity\Entity\LegacyEntityWrapperInterface;

/**
 * Implements \Drupal\ptools_entity\Repository\LegacyRepositoryManagerInterface.
 */
trait LegacyEntityWrapperRepositoryTrait {

  /**
   * @deprecated
   *
   * @see \Drupal\ptools_entity\Repository\LegacyEntityWrapperRepositoryInterface::wrap()
   */
  public function wrap(?EntityInterface $entity): ?LegacyEntityWrapperInterface {
    assert(!$entity || $entity instanceof LegacyEntityWrapperInterface);
    $wrapped = $entity ? $entity::wrap($entity) : NULL;
    assert(!$wrapped || $wrapped instanceof LegacyEntityWrapperInterface);
    return $wrapped;
  }

  /**
   * @deprecated
   *
   * @see \Drupal\ptools_entity\Repository\LegacyEntityWrapperRepositoryInterface::wrapMultiple()
   */
  public function wrapMultiple(array $entities): array {
    return array_map([$this, 'wrap'], $entities);
  }

}
