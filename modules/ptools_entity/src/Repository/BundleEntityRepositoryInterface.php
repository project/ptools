<?php

namespace Drupal\ptools_entity\Repository;

use Drupal\ptools_entity\Entity\BundleEntityInterface;

/**
 * Common interface for bundle entity repositories.
 */
interface BundleEntityRepositoryInterface {

  /**
   * Sets the bundle information for the current repository.
   *
   * @param string $id
   *   The bundle ID.
   * @param string $class
   *   The bundle class.
   */
  public function setBundleInfo(string $id, string $class): void;

  /**
   * Updates the specified wrapped entity.
   *
   * @param \Drupal\ptools_entity\Entity\BundleEntityInterface $entity
   *   A fieldable entity.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   *   If there was an error while updating the entity.
   */
  public function update(BundleEntityInterface $entity): void;

}
