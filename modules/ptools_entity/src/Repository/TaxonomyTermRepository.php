<?php

namespace Drupal\ptools_entity\Repository;

use Drupal\Core\Cache\Cache;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\ptools\StaticCacheTrait;
use Drupal\ptools_entity\TaxonomyTermStorageTrait;
use Drupal\taxonomy\TermInterface;

/**
 * A taxonomy term repository.
 */
class TaxonomyTermRepository {

  use StaticCacheTrait;
  use TaxonomyTermStorageTrait;

  /**
   * TaxonomyTermRepository constructor.
   */
  public function __construct(
    protected readonly Connection $database,
    protected readonly EntityTypeManagerInterface $entityTypeManager,
    protected readonly CacheBackendInterface $cacheBackend
  ) {
  }

  /**
   * Returns a taxonomy term ID matching the specified vocabulary and name.
   *
   * This assumes only one term with the specified name is available in the
   * specified vocabulary. If no vocabulary is specified the term name is
   * assumed to be unique across all vocabularies.
   *
   * @param string $name
   *   A term name.
   * @param string|null $vocabulary_name
   *   (optional) A vocabulary machine name.
   *
   * @return int|null
   *   A term ID or NULL if none could be found.
   */
  public function getTermIdByName(string $name, $vocabulary_name = NULL) {
    $cache = &$this->getStaticCache(__FUNCTION__, []);
    $id = &$cache[$name][$vocabulary_name ?: ''];

    if (!isset($id)) {
      $query = $this->database->select('taxonomy_term_field_data', 't')
        ->fields('t', ['tid'])
        ->condition('t.name', $name)
        ->range(0, 1);

      if ($vocabulary_name) {
        $query->condition('t.vid', $vocabulary_name);
      }

      $result = $query
        ->execute()
        ->fetchField();

      $id = $result ? (int) $result : 0;
    }

    return $id ?: NULL;
  }

  /**
   * Retrieves the complete ancestry of the specified term.
   *
   * @param \Drupal\taxonomy\TermInterface $term
   *   A taxonomy term object.
   *
   * @return int[][]
   *   An array of parent ID arrays.
   */
  public function getAncestry(TermInterface $term): array {
    $ids = [];
    foreach ($term->get('parent') as $item) {
      $parent_id = (int) $item->target_id;
      if ($parent_id) {
        $ids[$parent_id] = $parent_id;
      }
    }

    $info = $ids ? $this->getVocabularyHierarchyInfo($term->bundle()) : NULL;

    $level = 0;
    $ancestry = [$level => $ids];
    $parent_ids = &$ancestry[$level];

    while ($parent_ids) {
      $ids = &$ancestry[$level];
      $parent_ids = &$ancestry[++$level];
      $parent_ids = [];
      foreach ($ids as $id) {
        if (!empty($info[$id])) {
          $parent_ids += $info[$id];
        }
      }
    }

    if (!$parent_ids) {
      unset($ancestry[$level]);
    }

    return array_reverse($ancestry);
  }

  /**
   * Returns the full hierarchy info for the specified vocabulary.
   *
   * @param string $vocabulary_name
   *   The vocabulary machine name.
   *
   * @return int[][]
   *   An associative array of parent ID arrays keyed by term ID.
   */
  protected function getVocabularyHierarchyInfo(string $vocabulary_name): array {
    $stored_graph = &$this->getStaticCache(__FUNCTION__, []);
    $info = &$stored_graph[$vocabulary_name];

    if (!isset($info)) {
      $cid = 'ptools_entity:taxonomy_term:hierarchy:' . $vocabulary_name;
      $cache = $this->cacheBackend->get($cid);

      if ($cache) {
        $info = $cache->data;
        assert(is_array($info));
      }
      else {
        $info = [];

        $result = $this->database->select('taxonomy_term__parent', 'p')
          ->fields('p', ['entity_id', 'parent_target_id'])
          ->condition('p.bundle', $vocabulary_name)
          ->execute();

        foreach ($result as $row) {
          if ($row->parent_target_id) {
            $parent_id = (int) $row->parent_target_id;
            $info[(int) $row->entity_id][$parent_id] = $parent_id;
          }
        }

        $tags = ['taxonomy_term_list:' . $vocabulary_name];
        $this->cacheBackend->set($cid, $info, Cache::PERMANENT, $tags);
      }
    }

    return $info;
  }

}
