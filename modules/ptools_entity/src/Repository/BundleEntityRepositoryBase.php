<?php

declare(strict_types=1);

namespace Drupal\ptools_entity\Repository;

use Drupal\Component\Assertion\Inspector;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Entity\Sql\DefaultTableMapping;
use Drupal\Core\Entity\Sql\SqlEntityStorageInterface;
use Drupal\ptools\ExceptionLoggerTrait;
use Drupal\ptools\StaticCacheTrait;
use Drupal\ptools_entity\BasicEntityFieldHandlerTrait;
use Drupal\ptools_entity\Entity\BundleEntityInterface;

/**
 * Base class for database bundle entity repositories.
 */
abstract class BundleEntityRepositoryBase implements BundleEntityRepositoryInterface, LegacyEntityWrapperRepositoryInterface {

  use BasicEntityFieldHandlerTrait;
  use ExceptionLoggerTrait;
  use LegacyEntityWrapperRepositoryTrait;
  use StaticCacheTrait;

  /**
   * The bundle information fo the current repository.
   *
   * @var array
   */
  private array $bundleInfo;

  /**
   * The repository bundle ID.
   *
   * @deprecated
   */
  protected $bundle;

  /**
   * BundleEntityRepositoryBase constructor.
   */
  public function __construct(
    protected readonly Connection $database,
    protected readonly CacheBackendInterface $cacheBackend,
    protected readonly EntityTypeManagerInterface $entityTypeManager,
  ) {
  }

  /**
   * {@inheritDoc}
   */
  public function setBundleInfo(string $id, string $class): void {
    assert(is_subclass_of($class, BundleEntityInterface::class));
    if (!isset($this->bundleInfo)) {
      $this->bundle = $id;
      $this->bundleInfo['id'] = $id;
      $this->bundleInfo['class'] = $class;
    }
  }

  /**
   * Returns the repository entity type ID.
   *
   * @return string
   *   An entity type ID.
   */
  protected function getEntityTypeId(): string {
    assert(is_subclass_of($this->bundleInfo['class'], BundleEntityInterface::class));
    return $this->bundleInfo['class']::getBaseEntityTypeId();
  }

  /**
   * Returns the repository bundle ID.
   *
   * @return string
   *   A bundle ID.
   */
  protected function getBundleId(): string {
    return $this->bundleInfo['id'];
  }

  /**
   * Loads a bundle entity.
   *
   * @param string|int $id
   *   The entity ID.
   *
   * @return \Drupal\ptools_entity\Entity\BundleEntityInterface|null
   *   A bundle entity or NULL, if the entity could not be found.
   */
  protected function loadEntity(string|int $id): ?BundleEntityInterface {
    $entity = $this->getStorage($this->getEntityTypeId())->load($id) ?: NULL;
    assert(!isset($entity) || $entity instanceof BundleEntityInterface);
    return $entity;
  }

  /**
   * Loads the specified bundle entities.
   *
   * @param int[]|string[] $ids
   *   The entity IDs.
   *
   * @return \Drupal\ptools_entity\Entity\BundleEntityInterface[]
   *   An array of bundle entities.
   */
  protected function loadEntities(array $ids): array {
    $entities = $this->getStorage($this->getEntityTypeId())->loadMultiple($ids);
    assert(assert(Inspector::assertAllObjects($entities, BundleEntityInterface::class)));
    return $entities;
  }

  /**
   * Returns the entity table mapping.
   *
   * @param string|null $entity_type_id
   *   (optional) An entity type ID. Defaults to the repository entity type ID.
   *
   * @return \Drupal\Core\Entity\Sql\DefaultTableMapping
   *   A default table mapping instance.
   */
  protected function getTableMapping(string $entity_type_id = NULL): DefaultTableMapping {
    $storage = $this->getStorage($entity_type_id ?: $this->getEntityTypeId());
    assert($storage instanceof SqlEntityStorageInterface);
    $table_mapping = $storage->getTableMapping();
    assert($table_mapping instanceof DefaultTableMapping);
    return $table_mapping;
  }

  /**
   * Returns the referenced items.
   *
   * @param \Drupal\Core\Entity\FieldableEntityInterface[] $entities
   *   An array of fieldable entities.
   *
   * @return \Drupal\Core\Entity\EntityInterface[]
   *   An array of referenced entities.
   */
  protected function getReferencedItems(string $fieldName, array $entities): array {
    $referencedIds = [];
    $targetEntityTypeId = NULL;

    foreach ($entities as $entity) {
      assert($entity instanceof FieldableEntityInterface);
      $items = $entity->get($fieldName);
      if (!isset($targetEntityTypeId)) {
        $targetEntityTypeId = $items->getFieldDefinition()
          ->getSetting('target_type');
      }
      $id = $items->getValue()[0]['target_id'];
      if ($id) {
        $referencedIds[$id] = $id;
      }
    }

    if (!$referencedIds) {
      return [];
    }

    return $this->getStorage($targetEntityTypeId)
      ->loadMultiple($referencedIds);
  }

  /**
   * {@inheritdoc}
   */
  public function update(BundleEntityInterface $entity): void {
    $this->getStorage($entity::getBaseEntityTypeId())->save($entity);
  }

}
