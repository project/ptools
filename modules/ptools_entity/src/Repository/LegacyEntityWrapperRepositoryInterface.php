<?php

namespace Drupal\ptools_entity\Repository;

use Drupal\Core\Entity\EntityInterface;
use Drupal\ptools_entity\Entity\LegacyEntityWrapperInterface;

/**
 * Common interface for legacy entity wrappers.
 */
interface LegacyEntityWrapperRepositoryInterface {

  /**
   * Wraps the specified entity.
   *
   * @param \Drupal\Core\Entity\EntityInterface|null $entity
   *   The entity to be wrapped.
   *
   * @return \Drupal\ptools_entity\Entity\LegacyEntityWrapperInterface|null
   *   The wrapped entity.
   *
   * @deprecated
   */
  public function wrap(?EntityInterface $entity): ?LegacyEntityWrapperInterface;

  /**
   * Wraps the specified entities.
   *
   * @param \Drupal\Core\Entity\EntityInterface[] $entities
   *   The entities to be wrapped.
   *
   * @return \Drupal\ptools_entity\Entity\LegacyEntityWrapperInterface[]
   *   The wrapped entities.
   *
   * @deprecated
   */
  public function wrapMultiple(array $entities): array;

}
