<?php

declare(strict_types=1);

namespace Drupal\ptools_entity\Repository;

use Drupal\ptools\SelfServiceTrait;
use Drupal\ptools_entity\Entity\BundleEntityInterface;

/**
 * Bundle entity repository manager.
 */
final class RepositoryManager implements LegacyEntityWrapperRepositoryInterface {

  use LegacyEntityWrapperRepositoryTrait;
  use SelfServiceTrait;

  /**
   * The bundle entity repository information.
   *
   * array
   */
  protected array $repositoryInfo = [];

  /**
   * Registers a bundle entity repository.
   *
   * @param \Drupal\ptools_entity\Repository\BundleEntityRepositoryInterface $repository
   *   The bundle entity repository to be registered.
   * @param string $bundleId
   *   The bundle ID.
   * @param string $bundleClass
   *   The bundle class. This must implement
   *   \Drupal\ptools_entity\Entity\BundleEntityInterface.
   *
   * @internal
   */
  public function addRepository(BundleEntityRepositoryInterface $repository, string $bundleId, string $bundleClass): void {
    assert(is_subclass_of($bundleClass, BundleEntityInterface::class));
    $repository->setBundleInfo($bundleId, $bundleClass);

    $entityTypeId = $bundleClass::getBaseEntityTypeId();
    $this->repositoryInfo[$entityTypeId][$bundleId] = [
      'bundleClass' => $bundleClass,
      'repository' => $repository
    ];
  }

  /**
   * Returns the repository for the specified entity type and bundle.
   *
   * @param string $entityTypeId
   *   An entity type ID.
   * @param string $bundleId
   *   A bundle ID.
   *
   * @return \Drupal\ptools_entity\Repository\BundleEntityRepositoryInterface
   *   The bundle entity repository.
   *
   * @throws \InvalidArgumentException
   *   When the repository is not found.
   */
  public function getRepository(string $entityTypeId, string $bundleId): BundleEntityRepositoryInterface {
    $repository = $this->repositoryInfo[$entityTypeId][$bundleId]['repository'] ?? NULL;
    if ($repository) {
      return $repository;
    }
    throw new \InvalidArgumentException(sprintf("No repository found for \"%s\" entity type and \"%s\" bundle", $entityTypeId, $bundleId));
  }

  /**
   * Returns the matching repository for the specified entity.
   *
   * @param \Drupal\ptools_entity\Entity\BundleEntityInterface $entity
   *   A bundle entity.
   *
   * @return \Drupal\ptools_entity\Repository\BundleEntityRepositoryInterface
   *   A bundle entity repository.
   */
  public function getRepositoryFromEntity(BundleEntityInterface $entity): BundleEntityRepositoryInterface {
    return $this->getRepository($entity->getEntityTypeId(), $entity->bundle());
  }

  /**
   * Returns the repository info.
   *
   * @return array
   *   The repository info.
   *
   * @internal
   */
  public function getRepositoryInfo(): array {
    return $this->repositoryInfo;
  }

  /**
   * @deprecated
   * @see ::getRepository
   */
  final public function repository(string $entityTypeId, string $bundleId): BundleEntityRepositoryInterface {
    return $this->getRepository($entityTypeId, $bundleId);
  }

  /**
   * @deprecated
   * @see ::getRepository
   */
  final public function repositoryFromEntity(BundleEntityInterface $entity): BundleEntityRepositoryInterface {
    return $this->getRepositoryFromEntity($entity);
  }

}
